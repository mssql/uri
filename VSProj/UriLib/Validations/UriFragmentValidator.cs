using System;
using System.Collections.Generic;
using System.Text;
using UriLib.Support;

namespace UriLib.Validations {
	public static class UriFragmentValidator {
		public static ValidationResult<UriFragment> Validate(UriFragment fragment) {
			var ndxInvalid = IndexOfInvalidCharacter(fragment.Text);
			if (ndxInvalid >= 0) {
				return ValidationResult<UriFragment>.Error("Contains invalid character(s).");
			}

			return ValidationResult<UriFragment>.OK();
		}
		
		public static int IndexOfInvalidCharacter(StrSpan str) {
			var len = str.Length;
			if (len < 1) { return -1; }

			var ch0 = str[0];
			var startIndex = (ch0 == '#') ? 1 : 0;
			var result = UriConstants.Characters.IndexOfNonFChar(startIndex, str);
			return result;
		}
	}
}
