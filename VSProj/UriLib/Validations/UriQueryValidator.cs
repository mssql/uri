using System;
using System.Collections.Generic;
using System.Text;

namespace UriLib.Validations {
	public static class UriQueryValidator {
		public static ValidationResult<UriQuery> Validate(UriQuery query) {
			var parts = query.Parts;
			if (!(parts is null)) {
				var lenParts = parts.Count;
				for (var i = 0; i < lenParts; i++) {
					var part = parts[i];
					var rslt = part.Validate();

					string errMsg;
					if (rslt is null) {
						errMsg = null;
					}
					else if (rslt.IsError) {
						errMsg = rslt.ErrorMessage;
					}
					else {
						continue;
					}

					errMsg = errMsg ?? "Unkown error.";
					return ValidationResult<UriQuery>.Error($"The part on index '{i}' is not valid ({errMsg}).");
				}
			}

			return ValidationResult<UriQuery>.OK();
		}
	}
}
