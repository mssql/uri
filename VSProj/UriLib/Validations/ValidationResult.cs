using System;
using System.Collections.Generic;
using System.Text;

namespace UriLib.Validations {
	public sealed class ValidationResult<T> {
		public readonly bool IsError;
		public readonly string ErrorMessage;
		public readonly Func<T, T> Repair;
		public bool CanRepair => !(this.Repair is null);

		public ValidationResult(bool isError, string errorMessage, Func<T, T> repair) {
			this.IsError = isError;
			this.ErrorMessage = errorMessage;
			this.Repair = repair;
		}

		public static ValidationResult<T> OK() => new ValidationResult<T>(false, null, null);
		public static ValidationResult<T> Error(string message) => new ValidationResult<T>(true, message, null);
		public static ValidationResult<T> Error(string message, Func<T, T> repair) => new ValidationResult<T>(true, message, repair);
	}
}
