using System;
using System.Collections.Generic;
using System.Text;

namespace UriLib.Validations {
	public static class UriHostValidator {
		public static ValidationResult<UriHost> Validate(UriHost host) {
			var txt = host.Text;

			if (txt.IsEmpty) {
				return ValidationResult<UriHost>.OK();
			}

			if (txt.Contains(IsNotValidHostCharacter)) {
				return ValidationResult<UriHost>.Error("Contains invalid character(s).", RemoveInvalidCharacters);
			}

			return ValidationResult<UriHost>.OK();
		}

		public static bool IsNotValidHostCharacter(char ch) => !IsValidHostCharacter(ch);
		public static bool IsValidHostCharacter(char ch) {
			return UriConstants.Characters.IsUnreserved(ch)
					|| UriConstants.Characters.IsSubDelim(ch);
		}

		public static UriHost RemoveInvalidCharacters(UriHost host) {
			var txt = host.Text.Remove(IsNotValidHostCharacter);
			return new UriHost(txt);
		}
	}
}
