using System;
using System.Collections.Generic;
using System.Text;
using UriLib.Support;

namespace UriLib.Validations {
	public static class UriSchemeValidator {
		public static ValidationResult<UriScheme> Validate(UriScheme scheme) {
			var txt = scheme.Text;
			if (txt.Length < 1) {
				return ValidationResult<UriScheme>.OK();
			}

			if (!scheme.Suffix.IsEmpty) {
				return ValidationResult<UriScheme>.Error("Scheme can not have any suffix after terminator.", RemoveSuffix);
			}

			var name = scheme.Name;

			var ndxInvalid = name.IndexOf(IsNotValidSchemeNameCharacter);
			if (ndxInvalid >= 0) {
				// TODO: can we somewhat repair it?
				return ValidationResult<UriScheme>.Error("Contains invalid character(s).", RemoveInvalidCharacters);
			}

			var ch0 = name[0];
			if (ch0.IsWhiteSpace()) {
				return ValidationResult<UriScheme>.Error("Starts/end by whitespace.", TrimScheme);
			}

			var chLast = name[name.Length - 1];
			if (chLast.IsWhiteSpace()) {
				return ValidationResult<UriScheme>.Error("Starts/end by whitespace.", TrimScheme);
			}

			return ValidationResult<UriScheme>.OK();
		}

		public static bool IsNotValidSchemeNameCharacter(char ch) {
			return !IsValidSchemeNameCharacter(ch);
		}
		public static bool IsValidSchemeNameCharacter(char ch) {
			if (char.IsLetterOrDigit(ch)) { return true; }
			if (ch == '+' || ch == '.' || ch == '-') { return true; }

			return false;
		}

		public static UriScheme TrimScheme(UriScheme scheme) {
			var txt = scheme.Name.Trim();
			return new UriScheme(txt);
		}

		public static UriScheme RemoveSuffix(UriScheme scheme) {
			return UriScheme.FromParts(scheme.Name, scheme.Terminator, default(StrSpan));
		}

		public static UriScheme RemoveInvalidCharacters(UriScheme scheme) {
			var name = scheme.Name;
			var newName = name.Remove(IsNotValidSchemeNameCharacter);
			return UriScheme.FromParts(newName, scheme.Terminator, scheme.Suffix);
		}
	}
}
