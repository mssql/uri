using System;
using System.Collections.Generic;
using System.Text;
using UriLib.Support;

namespace UriLib.Validations {
	public static class UriPathValidator {
		public static ValidationResult<UriPath> Validate(UriPath host) {
			if (host.IsEmpty) {
				return ValidationResult<UriPath>.OK();
			}

			var parts = host.Parts;
			foreach (var part in parts) {
				if (part.Contains(IsNotValidPathPartCharacter)) {
					return ValidationResult<UriPath>.Error("Contains invalid character(s).", RemoveInvalidCharacters);
				}
			}

			return ValidationResult<UriPath>.OK();
		}

		public static bool IsNotValidPathPartCharacter(char ch) => !IsValidPathPartCharacter(ch);
		public static bool IsValidPathPartCharacter(char ch) {
			return ch == '@'
					|| UriConstants.Characters.IsUnreserved(ch)
					|| UriConstants.Characters.IsSubDelim(ch);
		}

		public static UriPath RemoveInvalidCharacters(UriPath host) {
			var oldParts = host.Parts;
			var lenParts = oldParts.Count;
			var newParts = new StrSpan[lenParts];
			for (var i = 0; i < lenParts; i++) {
				var oldPart = oldParts[i];
				var newPart = oldPart.Remove(IsNotValidPathPartCharacter);
				newParts[i] = newPart;
			}

			return new UriPath(newParts);
		}
	}
}
