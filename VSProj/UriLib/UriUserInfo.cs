using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UriLib.Support;
using UriLib.Validations;

namespace UriLib {
	/// <summary>
	/// Represents user-info part of <see cref="StructuredUri"/>.
	/// This structure is immutable and lazy.
	/// </summary>
	public partial struct UriUserInfo : IEquatable<UriUserInfo> {
		/// <summary>
		/// User name from user-info part of uri.
		/// </summary>
		public readonly StrSpan Name;

		/// <summary>
		/// Separator between name and password.
		/// </summary>
		public readonly char PasswordSeparator;

		/// <summary>
		/// User password from user-info part of uri.
		/// </summary>
		public readonly StrSpan Password;

		/// <summary>
		/// Separator after user-info part.
		/// </summary>
		public readonly char TerminalSeparator;

		/// <summary>
		/// Returns TRUE if user-info is empty.
		/// </summary>
		public bool IsEmpty => this.PasswordSeparator.IsEmpty() && this.TerminalSeparator.IsEmpty() && this.Name.IsEmpty && this.Password.IsEmpty;

		private ValidationResult<UriUserInfo> validationResult;
		public ValidationResult<UriUserInfo> Validate() => this.validationResult ?? (this.validationResult = UriUserInfoValidator.Validate(this));
		// if validation is null, we assume there is error
		public bool IsValid => !(this.Validate()?.IsError ?? true);

		public UriUserInfo(StrSpan name) {
			this.Name = name;
			this.PasswordSeparator = InternalExtensions.CHAR_ZERO;
			this.Password = default(StrSpan);
			this.TerminalSeparator = '@';
			this.hashCode = null;
			this.uriString = null;
			this.validationResult = null;
		}
		public UriUserInfo(StrSpan name, StrSpan password) {
			this.Name = name;
			this.PasswordSeparator = ':';
			this.Password = password;
			this.TerminalSeparator = '@';
			this.hashCode = null;
			this.uriString = null;
			this.validationResult = null;
		}
		public UriUserInfo(StrSpan name, char passwordSeparator, StrSpan password, char terminalSeparator) {
			this.Name = name;
			this.PasswordSeparator = passwordSeparator;
			this.Password = password;
			this.TerminalSeparator = terminalSeparator;
			this.hashCode = null;
			this.uriString = null;
			this.validationResult = null;
		}

		private string uriString;
		public string ToUriString() {
			var result = this.uriString;
			if (!(result is null)) { return result; }

			var sb = new StringBuilder();
			sb.Append(this.Name);

			var pwd = this.Password;
			if (!string.IsNullOrEmpty(pwd)) {
				sb.Append(this.PasswordSeparator).Append(pwd);
			}
			sb.Append(this.TerminalSeparator);

			this.uriString = result = sb.ToString();
			return result;
		}

		private int? hashCode;
		public override int GetHashCode() {
			var tmp = this.hashCode;
			if (tmp.HasValue) { return tmp.Value; }

			unchecked {
				var result = 17;
				result = result * 31 + this.Name.GetHashCode();
				result = result * 31 + this.PasswordSeparator.GetHashCode();
				result = result * 31 + this.Password.GetHashCode();
				result = result * 31 + this.TerminalSeparator.GetHashCode();

				this.hashCode = result;
				return result;
			}
		}

		public bool Equals(UriUserInfo other) {
			if (!this.Name.TextualyEquals(other.Name)) { return false; }
			if (!this.Name.TextualyEquals(other.Name)) { return false; }
			if (this.PasswordSeparator != other.PasswordSeparator) { return false; }
			if (this.TerminalSeparator != other.TerminalSeparator) { return false; }

			return true;
		}
	}

	partial struct UriUserInfo {
		#region Name
		public UriUserInfo WithName(StrSpan value) {
			return new UriUserInfo(value, this.PasswordSeparator, this.Password, this.TerminalSeparator);
		}
		public UriUserInfo WithoutName() => WithName(default(StrSpan));
		#endregion Name

		#region PasswordSeparator
		public UriUserInfo WithPasswordSeparator(char value) {
			return new UriUserInfo(this.Name, value, this.Password, this.TerminalSeparator);
		}
		public UriUserInfo WithoutPasswordSeparator() => WithPasswordSeparator(InternalExtensions.CHAR_ZERO);
		#endregion PasswordSeparator

		#region Password
		public UriUserInfo WithPassword(StrSpan value) {
			return new UriUserInfo(this.Name, this.PasswordSeparator, value, this.TerminalSeparator);
		}
		public UriUserInfo WithoutPassword() => WithPassword(default(StrSpan));
		#endregion Password

		#region TerminalSeparator
		public UriUserInfo WithTerminalSeparator(char value) {
			return new UriUserInfo(this.Name, this.PasswordSeparator, this.Password, this.TerminalSeparator);
		}
		public UriUserInfo WithoutTerminalSeparator() => WithTerminalSeparator(InternalExtensions.CHAR_ZERO);
		#endregion TerminalSeparator

		#region Name & Password
		public UriUserInfo WithNameAndPassword(StrSpan name, StrSpan password) {
			return new UriUserInfo(name, this.PasswordSeparator, password, this.TerminalSeparator);
		}
		public UriUserInfo WithoutNameAndPassword() => WithNameAndPassword(default(StrSpan), default(StrSpan));
		#endregion Name & Password
	}

	#region Serialization
	partial struct UriUserInfo {
		public static class Serialization {
			public static void Serialize(ref UriUserInfo value, BinaryWriter writer) {
				writer.Write((byte)1);
				V1.Serialize(ref value, writer);
			}
			public static UriUserInfo Deserialize(BinaryReader reader) {
				var version = reader.ReadByte();
				if (0 == version) {
					// null
					return default(UriUserInfo);
				}
				if (1 == version) {
					return V1.Deserialize(reader);
				}

				throw new NotSupportedException($"The serialization viersion '{version}' is not supported.");
			}

			public static class V1 {
				public static void Serialize(ref UriUserInfo value, BinaryWriter writer) {
					var name = value.Name.Text;
					var pwdSep = value.PasswordSeparator;
					var pwd = value.Password.Text;
					var terSep = value.TerminalSeparator;

					byte mask = 0;

					var nameIsNull = name is null;
					var pwdSepIsNull = pwdSep.IsEmpty();
					var pwdIsNull = pwd is null;
					var terSepIsNull = terSep.IsEmpty();

					if (nameIsNull) { mask |= 1; }
					if (pwdSepIsNull) { mask |= 2; }
					if (pwdIsNull) { mask |= 4; }
					if (terSepIsNull) { mask |= 8; }

					writer.Write(mask);
					if (!nameIsNull) { writer.Write(name); }
					if (!pwdSepIsNull) { writer.Write(pwdSep); }
					if (!pwdIsNull) { writer.Write(pwd); }
					if (!terSepIsNull) { writer.Write(terSep); }
				}
				public static UriUserInfo Deserialize(BinaryReader reader) {
					var mask = reader.ReadByte();

					var nameIsNull = ((mask & 1) == 1);
					var pwdSepIsNull = ((mask & 2) == 2);
					var pwdIsNull = ((mask & 4) == 4);
					var terSepIsNull = ((mask & 8) == 8);

					var name = nameIsNull ? null : reader.ReadString();
					var pwdSeparator = pwdSepIsNull ? (char)0 : reader.ReadChar();
					var password = pwdIsNull ? null : reader.ReadString();
					var terminator = terSepIsNull ? (char)0 : reader.ReadChar();

					return new UriUserInfo(name, pwdSeparator, password, terminator);
				}
			}
		}
	}
	#endregion Serialization
}
