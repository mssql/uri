using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UriLib.Support;
using UriLib.Validations;

namespace UriLib {
	/// <summary>
	/// Represent query part of <see cref="Url"/>.
	/// This structure is immutable and lazy.
	/// </summary>
	public partial struct UriQuery {
		private StrSpan? text;
		public StrSpan Text {
			get {
				var result = this.text;
				if (result.HasValue) { return result.Value; }

				var lst = this.parts;
				if (lst is null || lst.Count < 1) {
					this.text = result = default(StrSpan);
				}

				this.text = result = ComposeText(lst);
				return result.Value;
			}
		}

		private ReadOnlyArray<UriQueryPart> parts;
		public ReadOnlyArray<UriQueryPart> Parts {
			get {
				var result = this.parts;
				if (!(result is null)) { return result; }

				var txt = this.text;
				if (!txt.HasValue) { return null; }

				var span = txt.Value;
				if (span.IsEmpty) { return null; }

				var lst = SplitToParts(span);
				result = new ReadOnlyArray<UriQueryPart>(lst);
				this.parts = result;

				return result;
			}
		}

		public int PartsCount => this.Parts?.Count ?? 0;

		public bool IsEmpty => string.IsNullOrEmpty(this.text) && 1 > (this.parts?.Count ?? 0);

		private ValidationResult<UriQuery> validationResult;
		public ValidationResult<UriQuery> Validate() => this.validationResult ?? (this.validationResult = UriQueryValidator.Validate(this));
		// if validation is null, we assume there is error
		public bool IsValid => !(this.Validate()?.IsError ?? true);

		public UriQuery(StrSpan text) {
			this.text = text;
			this.parts = null;
			this.hashCode = null;
			this.validationResult = null;
		}
		public UriQuery(params UriQueryPart[] parts) : this() {
			this.text = null;
			if (parts is null || parts.Length < 1) {
				this.parts = null;
			}
			else {
				var clonedParts = (UriQueryPart[])parts.Clone();
				this.parts = new ReadOnlyArray<UriQueryPart>(clonedParts);
			}
			this.validationResult = null;
		}

		public override bool Equals(object obj) {
			if (obj is UriQuery query) {
				return this.Equals(query);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}
			else {
				return false;
			}
		}

		private int? hashCode;
		public override int GetHashCode() {
			var result = this.hashCode;
			if (result.HasValue) { return result.Value; }

			// TODO: can be optimized without changing the result?
			this.hashCode = result = this.Text.GetHashCode();
			return result.Value;
		}

		public override string ToString() => this.ToUriString();

		public string ToUriString() => this.Text;

		public static string ComposeText(IReadOnlyList<UriQueryPart> parts) {
			if (parts is null) { return null; }

			var source = parts.GetEnumerator();
			if (!source.MoveNext()) {
				return null;
			}

			var sb = new StringBuilder();
			source.Current.Write(sb, '?');
			while (source.MoveNext()) {
				var part = source.Current;
				var separator = part.Separator;
				if (separator.IsEmpty()) { separator = '&'; }
				part.Write(sb, separator);
			}

			return sb.ToString();
		}

		public static UriQueryPart[] SplitToParts(StrSpan span) {
			var query = span.SkipUntil(ch => ch != '?');
			var buffer = query;

			var count = buffer.CountOf(UriQueryPart.IsPartSeparator);
			var result = new UriQueryPart[count];
			for (var i = 0; i < count; i++) {
				var part = UriQueryPart.Parse(buffer, out var lastIndex);
				result[i] = part;
				buffer = buffer.Skip(lastIndex);
			}

			return result;
		}
	}

	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	partial struct UriQuery {
		private string DebuggerDisplay => this.Text;
	}

	partial struct UriQuery {
		public IEnumerable<string> GetQueryValuesByKey(string key) {
			var source = this.Parts;
			if (source is null) { yield break; }

			foreach (var part in source) {
				if (0 == string.Compare(key, part.Key, StringComparison.Ordinal)) {
					yield return part.Value;
				}
			}
		}
	}

	partial struct UriQuery : IEquatable<UriQuery> {
		public bool Equals(UriQuery other) {
			// TODO: can be optimized
			return this.Equals(other.Text);
		}
	}

	partial struct UriQuery : IEquatable<string> {
		public bool Equals(string other) => this.Text.TextualyEquals(other);
	}

	partial struct UriQuery : IEquatable<StrSpan> {
		public bool Equals(StrSpan other) => this.Text.TextualyEquals(other);
	}

	partial struct UriQuery {
		public UriQueryPart? FindQueryPart(string key) {
			return this.FindQueryPart(key, StringComparison.Ordinal);
		}
		public UriQueryPart? FindQueryPart(string key, StringComparison keyComparision) {
			var parts = this.Parts;
			if (parts is null) { return null; }

			foreach (var part in parts) {
				// TODO: can be optimized
				if (0 == string.Compare(key, part.Key, keyComparision)) {
					return part;
				}
			}

			return null;
		}
	}

	partial struct UriQuery {
		public static void Serialize(ref UriQuery value, BinaryWriter writer) {
			writer.Write((byte)1);
			V1.Serialize(ref value, writer);
		}
		public static UriQuery Deserialize(BinaryReader reader) {
			var version = reader.ReadByte();
			if (0 == version) {
				// null
				return default(UriQuery);
			}
			if (1 == version) {
				return V1.Deserialize(reader);
			}

			throw new NotSupportedException($"The serialization viersion '{version}' is not supported.");
		}

		public static class V1 {
			public static void Serialize(ref UriQuery value, BinaryWriter writer) {
				// TODO: can be optimized for case where parts are available
				var str = value.Text.Text;
				writer.Write(str);
			}
			public static UriQuery Deserialize(BinaryReader reader) {
				var str = reader.ReadString();
				return new UriQuery(str);
			}
		}
	}
}
