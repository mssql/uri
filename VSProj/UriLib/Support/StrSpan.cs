using System;
using System.Diagnostics;

namespace UriLib.Support {
	/// <summary>
	/// Represents span of the string.
	/// </summary>
	[DebuggerDisplay("{Text}")]
	public partial struct StrSpan : IEquatable<StrSpan>, IEquatable<string> {
		public readonly string Original;
		public readonly int StartIndex;
		public readonly int Length;

		private string text;
		public string Text => text ?? (text = CreateText(this.Original, this.StartIndex, this.Length));

		public char this[int index] => this.Original[this.StartIndex + index];

		public bool IsEmpty {
			get {
				// TODO: can be optimalized
				return string.IsNullOrEmpty(this.Text);
			}
		}

		public StrSpan(string original) {
			this.Original = original;

			if (original is null) {
				this.StartIndex = -1;
				this.Length = -1;
			}
			else {
				this.StartIndex = 0;
				this.Length = original.Length;
			}

			this.text = null;
		}
		public StrSpan(string original, int startIndex) {
			this.Original = original;
			this.StartIndex = startIndex;
			this.Length = (original?.Length ?? 0) - startIndex;
			this.text = null;
		}
		public StrSpan(string original, int startIndex, int length) {
			this.Original = original;
			this.StartIndex = startIndex;
			this.Length = length;
			this.text = null;
		}

		public override string ToString() => this.Text;
		public string ToStringOrNull() {
			if (this.Length < 1) { return null; }

			return this.ToString();
		}

		public override int GetHashCode() {
			// TODO: can be optimalized
			return this.Text?.GetHashCode() ?? 0;
		}
		public override bool Equals(object obj) {
			if (obj is StrSpan span) {
				return this.Equals(span);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}

			return false;
		}

		public bool TextualyEquals(StrSpan other) {
			// TODO: can be optimalized
			var rslt = string.CompareOrdinal(this.Text, other.Text);
			return (0 == rslt);
		}
		public bool TextualyEquals(string other) {
			// TODO: can be optimalized
			var rslt = string.CompareOrdinal(this.Text, other);
			return (0 == rslt);
		}

		public bool Equals(StrSpan other) {
			if (this.StartIndex != other.StartIndex) { return false; }
			if (this.Length != other.Length) { return false; }
			if (0 != string.CompareOrdinal(this.Original, other.Original)) { return false; }

			return true;
		}

		public bool Equals(string other) {
			return this.TextualyEquals(other);
		}

		public static string CreateText(string original, int startIndex, int length) {
			if (original is null) { return null; }

			var totalLength = original.Length;
			if (startIndex >= totalLength) { return string.Empty; }

			var okStartIndex = startIndex;
			var okLength = length;
			if (okStartIndex < 0) {
				okLength += okStartIndex;
				okStartIndex = 0;
			}

			if (okLength < 0) { return null; }

			return original.Substring(okStartIndex, okLength);
		}

		public static implicit operator StrSpan(string value) => new StrSpan(value);
		public static implicit operator StrSpan(char ch) => new StrSpan(ch.ToString());

		public static implicit operator string(StrSpan value) => value.ToString();
	}

	partial struct StrSpan {
		public static StrSpan[] Split(string source, char ch) {
			if (string.IsNullOrEmpty(source)) { return null; }

			// TODO: can be optimalized
			var parts = source.Split(ch);
			var result = new StrSpan[parts.Length];
			for (var i = 0; i < parts.Length; i++) {
				result[i] = parts[i];
			}

			return result;
		}
	}

	partial struct StrSpan {
		// TODO: can be optimized
		public int IndexOf(string str) => this.Text?.IndexOf(str) ?? -1;

		public int IndexOf(Func<char, bool> predicate) => this.IndexOf(0, predicate);
		public int IndexOf(int startIndex, Func<char, bool> predicate) {
			var len = this.Length;
			if (len <= startIndex) { return -1; }

			var src = this.Original;
			var start = this.StartIndex;
			var max = start + len;
			for (var i = this.StartIndex + startIndex; i < max; i++) {
				var ch = src[i];
				if (predicate(ch)) {
					return i - start;
				}
			}

			return -1;
		}
		public bool Contains(Func<char, bool> predicate) => this.IndexOf(predicate) >= 0;
	}

	partial struct StrSpan {
		public StrSpan Trim() => this.TrimStart().TrimEnd();

		public StrSpan TrimStart() {
			var orig = this.Original;
			if (orig is null) { return this; }

			var selfLen = this.Length;
			for (var i = 0; i < selfLen; i++) {
				if (!char.IsWhiteSpace(orig, i)) {
					var len = selfLen - i;
					return new StrSpan(orig, i, len);
				}
			}

			return default(StrSpan);
		}

		public StrSpan TrimEnd() {
			var orig = this.Original;
			if (orig is null) { return this; }

			var selfStart = this.StartIndex;
			for (var i = selfStart + this.Length - 1; i >= selfStart; i--) {
				if (!char.IsWhiteSpace(orig, i)) {
					var len = i - selfStart + 1;
					return new StrSpan(orig, selfStart, len);
				}
			}

			return default(StrSpan);
		}
	}

	partial struct StrSpan {
		public StrSpan Remove(Func<char, bool> predicate) {
			if (this.IsEmpty) { return this; }

			// TODO: need optimization
			var lenSelf = this.Length;
			var buffer = new char[lenSelf];
			var counter = 0;
			for (var i = 0; i < lenSelf; i++) {
				var ch = this[i];
				if (predicate(ch)) { continue; }

				buffer[counter] = ch;
				counter++;
			}

			if (counter < 1) { return default(StrSpan); }

			var str = new string(buffer, 0, counter);
			return str;
		}
	}

	partial struct StrSpan {
		public StrSpan Until(string str) {
			if (string.IsNullOrEmpty(str)) { return this; }

			var ndxStr = this.IndexOf(str);
			if (ndxStr <= 0) { return this; }

			return new StrSpan(this.Original, this.StartIndex, ndxStr);
		}

		public StrSpan Until(Func<char, bool> predicate) {
			var len = this.Length;
			var start = this.StartIndex;

			var ndx = 0;
			for (; ndx < len; ndx++) {
				var ch = this.Original[ndx + start];
				if (!predicate(ch)) { break; }
			}

			return new StrSpan(this.Original, start, ndx);
		}
	}

	partial struct StrSpan {
		public StrSpan Skip(int length) {
			return new StrSpan(this.Original, this.StartIndex + length, this.Length - length);
		}
	}

	partial struct StrSpan {
		public StrSpan SkipUntil(Func<char, bool> predicate) {
			var len = this.Length;
			var start = this.StartIndex;

			var ndx = 0;
			for (; ndx < len; ndx++) {
				var ch = this.Original[ndx + start];
				if (!predicate(ch)) { break; }
			}

			return new StrSpan(this.Original, start + ndx, len - ndx);
		}
	}

	partial struct StrSpan {
		public int CountOf(Func<char, bool> clasifier) {
			int result = 0;

			var len = this.Length;
			var offset = this.StartIndex;
			for (var i = 0; i < len; i++) {
				var ch = this.Original[i + offset];
				if (clasifier(ch)) {
					result++;
				}
			}

			return result;
		}
	}
}