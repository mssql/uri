using System;
using System.Globalization;

namespace UriLib.Support {
	/// <summary>
	/// Represents integer based on string.
	/// </summary>
	public partial struct StrInt {
		public readonly StrSpan Source;
		public readonly int? Value;

		public bool IsEmpty => !this.Value.HasValue && this.Source.IsEmpty;

		public StrInt(StrSpan source, int? value) {
			this.Source = source;
			this.Value = value;
		}

		public StrInt(StrSpan source) {
			this.Source = source;

			if (int.TryParse(source.Text, NumberStyles.Integer, CultureInfo.InvariantCulture, out var number)) {
				this.Value = number;
			}
			else {
				this.Value = null;
			}
		}

		public StrInt(int? value) {
			this.Value = value;
			if (value.HasValue) {
				this.Source = value.Value.ToString(null, CultureInfo.InvariantCulture);
			}
			else {
				this.Source = default(StrSpan);
			}
		}

		public override string ToString() {
			var num = this.Value;
			if (num.HasValue) { return num.Value.ToString(null, CultureInfo.InvariantCulture); }

			return this.Source.ToString();
		}

		public string ToStringOrNull() {
			var num = this.Value;
			if (num.HasValue) { return num.Value.ToString(null, CultureInfo.InvariantCulture); }

			return this.Source.ToStringOrNull();
		}
	}

	partial struct StrInt {
		public static implicit operator StrInt(StrSpan str) => new StrInt(str);
		public static implicit operator StrInt(string str) => new StrInt(str);
		public static implicit operator StrInt(int? number) => new StrInt(number);
		public static implicit operator StrInt(int number) => new StrInt(number);

		public static implicit operator StrSpan(StrInt value) => value.Source;
		public static implicit operator string(StrInt value) => value.ToString();
		public static implicit operator int? (StrInt value) => value.Value;
		public static explicit operator int (StrInt value) => value.Value.Value;
	}

	partial struct StrInt {
		public override bool Equals(object obj) {
			if (obj is null) {
				return (this.Source.Original is null);
			}
			else if (obj is StrInt sint) {
				return this.Equals(sint);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}
			else if (obj is StrSpan span) {
				return this.Equals(span);
			}
			else if (obj is int num) {
				return this.Equals(num);
			}

			return false;
		}
		public override int GetHashCode() => this.Source.GetHashCode();
	}

	partial struct StrInt : IEquatable<StrInt> {
		public bool Equals(StrInt other) {
			var selfValue = this.Value;
			var otherValue = other.Value;

			if (selfValue.HasValue != otherValue.HasValue) {
				return false;
			}
			else if (selfValue.HasValue) {
				return (selfValue.Value == otherValue.Value);
			}

			return this.Source.TextualyEquals(other.Source);
		}
	}

	partial struct StrInt : IEquatable<int> {
		public bool Equals(int other) {
			var selfValue = this.Value;

			if (selfValue.HasValue) {
				return (selfValue.Value == other);
			}

			return false;
		}
	}

	partial struct StrInt : IEquatable<StrSpan> {
		public bool Equals(StrSpan other) {
			return this.Source.TextualyEquals(other);
		}
	}

	partial struct StrInt : IEquatable<string> {
		public bool Equals(string other) {
			return this.Source.TextualyEquals(other);
		}
	}
}