using System.Collections.Generic;
using System.Text;

namespace UriLib.Support {
	/// <summary>
	/// Extension methods for internal use.
	/// </summary>
	public static class InternalExtensions {
		public const char CHAR_ZERO = (char)0;

		public static bool IsEmpty(this char ch) => CHAR_ZERO == ch;
		public static bool IsWhiteSpace(this char ch) => char.IsWhiteSpace(ch);

		public static string ToJsonArray(this IEnumerable<string> jsonArray) {
			StringBuilder result = new StringBuilder();
			result.Append("[");
			var isFirst = true;
			foreach (var queryValue in jsonArray) {
				if (isFirst) {
					isFirst = false;
				}
				else {
					result.Append(",");
				}
				result.Append("\"").Append(queryValue.Replace("\"", "\\\"")).Append("\"");
			}
			result.Append("]");
			return result.ToString();
		}

	}
}