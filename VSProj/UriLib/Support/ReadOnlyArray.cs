using System;
using System.Collections;
using System.Collections.Generic;

namespace UriLib.Support {
	/// <summary>
	/// Read-only wrapper around passed array.
	/// </summary>
	public partial class ReadOnlyArray<T> {
		private readonly T[] source;

		public ReadOnlyArray() : this(null) { }
		public ReadOnlyArray(T[] source) {
			this.source = source ?? Array.Empty<T>();
		}
	}

	partial class ReadOnlyArray<T> : IReadOnlyList<T> {
		public T this[int index] => this.source[index];

		public int Count => this.source.Length;

		public IEnumerator<T> GetEnumerator() => ((IReadOnlyList<T>)this.source).GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => ((IReadOnlyList<T>)this.source).GetEnumerator();
	}
}