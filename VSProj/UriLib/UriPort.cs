using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UriLib.Support;

namespace UriLib {
	/// <summary>
	/// Represents port part of <see cref="Url"/>.
	/// This structure is immutable and lazy.
	/// </summary>
	public partial struct UriPort {
		public readonly StrInt Source;

		public bool IsEmpty => this.Source.IsEmpty;
		public bool IsNumber => this.Source.Value.HasValue;
		public bool IsValid => this.IsEmpty || this.IsNumber;

		public UriPort(StrInt source) {
			this.Source = source;
		}

		public string ToUriString() {
			var result = this.Source.ToStringOrNull();
			if (result is null) { return null; }

			return ":" + result;
		}

		public override string ToString() => this.Source.ToString();
		public string ToStringOrNull() => this.Source.ToStringOrNull();

		public override int GetHashCode() => this.Source.GetHashCode();
		public override bool Equals(object obj) {
			if (obj is null) {
				return (this.Source.Source.Original is null);
			}
			else if (obj is UriPort port) {
				return this.Equals(port);
			}
			else if (obj is StrInt sint) {
				return this.Equals(sint);
			}
			else if (obj is StrSpan span) {
				return this.Equals(span);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}
			else if (obj is int num) {
				return this.Equals(num);
			}

			return false;
		}
	}

	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	partial struct UriPort {
		private string DebuggerDisplay => this.ToUriString();
	}

	partial struct UriPort {
		public static implicit operator UriPort(StrInt value) => new UriPort(value);
		public static implicit operator UriPort(StrSpan value) => new UriPort(value);
		public static implicit operator UriPort(string value) => new UriPort(value);
		public static implicit operator UriPort(int? value) => new UriPort(value);

		public static implicit operator StrInt(UriPort value) => value.Source;
		public static implicit operator StrSpan(UriPort value) => value.Source.Source;
		public static implicit operator string(UriPort value) => value.ToUriString();
		public static implicit operator int? (UriPort value) => value.Source.Value;
	}

	partial struct UriPort : IEquatable<UriPort> {
		public bool Equals(UriPort other) {
			return this.Source.Equals(other.Source);
		}
	}

	partial struct UriPort : IEquatable<StrInt> {
		public bool Equals(StrInt other) {
			return this.Source.Equals(other);
		}
	}

	partial struct UriPort : IEquatable<StrSpan> {
		public bool Equals(StrSpan other) {
			return this.Source.Equals(other);
		}
	}

	partial struct UriPort : IEquatable<string> {
		public bool Equals(string other) {
			return this.Source.Equals(other);
		}
	}

	partial struct UriPort : IEquatable<int> {
		public bool Equals(int other) {
			return this.Source.Equals(other);
		}
	}

	partial struct UriPort {
		public static void Serialize(ref UriPort value, BinaryWriter writer) {
			writer.Write((byte)1);
			V1.Serialize(ref value, writer);
		}
		public static UriPort Deserialize(BinaryReader reader) {
			var version = reader.ReadByte();
			if (0 == version) {
				// null
				return default(UriPort);
			}
			if (1 == version) {
				return V1.Deserialize(reader);
			}

			throw new NotSupportedException($"The serialization viersion '{version}' is not supported.");
		}

		public static class V1 {
			public static void Serialize(ref UriPort value, BinaryWriter writer) {
				var str = value.Source.Source.Text;
				writer.Write(str);
			}
			public static UriPort Deserialize(BinaryReader reader) {
				var str = reader.ReadString();
				return new UriPort(str);
			}
		}
	}
}
