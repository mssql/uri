using System;
using UriLib.Support;

namespace UriLib {
	/// <summary>
	/// Contains constant members for StructuredUri.
	/// </summary>
	public static class UriConstants {
		/// <summary>
		/// Contains constant members related to characters.
		/// </summary>
		public static class Characters {
			public const string GenDelims = ":/?#[]@";
			public static bool IsGenDelim(char ch) => GenDelims.IndexOf(ch) >= 0;

			public const string SubDelims = "!$&'()*+,;=";
			public static bool IsSubDelim(char ch) => SubDelims.IndexOf(ch) >= 0;

			public const string Reserved = GenDelims + SubDelims;
			public static bool IsReserved(char ch) => Reserved.IndexOf(ch) >= 0;

			public const string UnreservedSymbols = "-._~";
			public static bool IsUnreserved(char ch) {
				if (char.IsLetterOrDigit(ch)) { return true; }
				if (UnreservedSymbols.IndexOf(ch) >= 0) { return true; }

				return false;
			}

			public static bool IsPctEncoded(StrSpan str, int index, out int length) {
				var ch0 = str[index];
				if (ch0 != '%') {
					length = 0;
					return false;
				}

				var startIndex = index + 1;
				var ndxNonDigit = str.IndexOf(startIndex, x => !char.IsDigit(x));
				if (ndxNonDigit < 0) {
					// all following characters are ok
					ndxNonDigit = str.Length - 1;
				}

				var len = ndxNonDigit - startIndex;
				if (len < 1) {
					length = 0;
					return false;
				}

				length = len + 1;
				return true;
			}

			private static int IndexOfNonCharacterAllowingPctEncoding(int startIndex, StrSpan str, Func<char, bool> predicate) {
				var len = str.Length;
				for (var i = startIndex; i < len; i++) {
					if (IsPctEncoded(str, i, out var lenPct)) {
						i += lenPct - 1;
						continue;
					}

					var ch = str[i];
					if (!predicate(ch)) {
						return i;
					}
				}

				// no non-PChar character was found
				return -1;
			}

			public static bool IsPChar(char ch) {
				// RFC: pchar = unreserved / pct-encoded / sub-delims / ":" / "@"
				return ':' == ch
						|| '@' == ch
						|| IsUnreserved(ch)
						|| IsSubDelim(ch);
			}

			public static int IndexOfNonPChar(StrSpan str) => IndexOfNonPChar(0, str);
			public static int IndexOfNonPChar(int startIndex, StrSpan str) {
				return IndexOfNonCharacterAllowingPctEncoding(startIndex, str, IsPChar);
				//var len = str.Length;
				//for (var i = startIndex; i < len; i++) {
				//	if (IsPctEncoded(str, i, out var lenPct)) {
				//		i += lenPct - 1;
				//		continue;
				//	}

				//	var ch = str[i];
				//	if (!IsPChar(ch)) {
				//		return i;
				//	}
				//}

				//// no non-PChar character was found
				//return -1;
			}

			public static bool IsFChar(char ch) {
				return ch == '/'
						|| ch == '?'
						|| IsPChar(ch);
			}
			public static int IndexOfNonFChar(int startIndex, StrSpan str) {
				return IndexOfNonCharacterAllowingPctEncoding(startIndex, str, IsFChar);
			}
		}
	}
}