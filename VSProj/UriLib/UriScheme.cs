using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UriLib.Support;
using UriLib.Validations;

namespace UriLib {
	/// <summary>
	/// Represents scheme part of <see cref="Url"/>.
	/// This structure is immutable and lazy.
	/// </summary>
	public partial struct UriScheme {
		private static readonly string TERMINATOR = "://";

		public readonly StrSpan Text;

		private bool isParsed;
		private void Parse() {
			if (this.isParsed) { return; }

			SplitToParts(this.Text, out this.name, out this.terminator, out this.suffix);
		}

		private StrSpan name;
		public StrSpan Name {
			get {
				this.Parse();
				return this.name;
			}
		}

		private StrSpan terminator;
		public StrSpan Terminator {
			get {
				this.Parse();
				return this.terminator;
			}
		}

		private StrSpan suffix;
		public StrSpan Suffix {
			get {
				this.Parse();
				return this.suffix;
			}
		}

		public bool IsEmpty => this.Text.IsEmpty || (this.Name.IsEmpty && this.Suffix.IsEmpty);
		

		private ValidationResult<UriScheme> validationResult;
		public ValidationResult<UriScheme> Validate() => this.validationResult ?? (this.validationResult = UriSchemeValidator.Validate(this));
		// if validation is null, we assume there is error
		public bool IsValid => !(this.Validate()?.IsError ?? true);

		public UriScheme(StrSpan text) {
			this.Text = text;
			this.isParsed = false;
			this.name = null;
			this.terminator = null;
			this.suffix = null;
			this.validationResult = null;
		}
		public UriScheme(StrSpan text, StrSpan name, StrSpan terminator, StrSpan suffix) {
			this.Text = text;
			this.isParsed = true;
			this.name = name;
			this.terminator = terminator;
			this.suffix = suffix;
			this.validationResult = null;
		}
		public static UriScheme FromParts(StrSpan name) {
			return new UriScheme(name, name, default(StrSpan), default(StrSpan));
		}
		public static UriScheme FromParts(StrSpan name, StrSpan terminator) {
			var text = name + terminator;
			return new UriScheme(text, name, terminator, default(StrSpan));
		}
		public static UriScheme FromParts(StrSpan name, StrSpan terminator, StrSpan suffix) {
			var text = name + terminator + suffix;
			return new UriScheme(text, name, terminator, suffix);
		}

		public string ToUriString() {
			var name = this.Name;
			if (name.IsEmpty) { return null; }

			return name.Text + TERMINATOR;
		}
		public override string ToString() => this.ToUriString();

		public override int GetHashCode() => this.Text.GetHashCode();
		public override bool Equals(object obj) {
			if (obj is null) {
				return (this.Text.Original is null);
			}
			else if (obj is UriScheme scheme) {
				return this.Equals(scheme);
			}
			else if (obj is StrSpan span) {
				return this.Equals(span);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}

			return false;
		}
	}

	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	partial struct UriScheme {
		private string DebuggerDisplay => this.ToUriString();
	}

	partial struct UriScheme {
		public static implicit operator string(UriScheme value) => value.ToUriString();
		public static implicit operator UriScheme(StrSpan value) => new UriScheme(value);
		public static implicit operator UriScheme(string value) => new UriScheme(value);
	}

	partial struct UriScheme : IEquatable<UriScheme> {
		public bool Equals(UriScheme other) {
			if (this.Text.TextualyEquals(other.Text)) {
				return true;
			}

			if (this.Name.TextualyEquals(other.Name)) {
				return true;
			}

			return false;
		}
	}

	partial struct UriScheme : IEquatable<StrSpan> {
		public bool Equals(StrSpan other) {
			if (this.Text.TextualyEquals(other)) {
				return true;
			}

			if (this.Name.TextualyEquals(other)) {
				return true;
			}

			if (other.TextualyEquals(this.ToUriString())) {
				return true;
			}

			return false;
		}
	}

	partial struct UriScheme : IEquatable<string> {
		public bool Equals(string other) => this.Equals((StrSpan)other);
	}

	partial struct UriScheme {
		public static StrSpan GetName(StrSpan text) {
			return text.Until(TERMINATOR);
		}

		public static void SplitToParts(StrSpan text, out StrSpan name, out StrSpan terminator, out StrSpan suffix) {
			name = text.Until(TERMINATOR);

			var lenName = name.Length;
			var lenText = text.Length;
			if (lenName < lenText) {
				// there is no terminator => whole text is the name of scheme
				terminator = default(StrSpan);
				suffix = default(StrSpan);
			}
			else {
				terminator = TERMINATOR;
				var ndxSuffix = lenName + terminator.Length;
				if (ndxSuffix < lenText) {
					suffix = text.Skip(ndxSuffix);
				}
				else {
					suffix = default(StrSpan);
				}
			}
		}
	}

	partial struct UriScheme {
		public static class Serialization {
			public static void Serialize(ref UriScheme value, BinaryWriter writer) {
				writer.Write((byte)1);
				V1.Serialize(ref value, writer);
			}
			public static UriScheme Deserialize(BinaryReader reader) {
				var version = reader.ReadByte();
				if (0 == version) {
					// null
					return default(UriScheme);
				}
				if (1 == version) {
					return V1.Deserialize(reader);
				}

				throw new NotSupportedException($"The serialization viersion '{version}' is not supported.");
			}

			public static class V1 {
				public static void Serialize(ref UriScheme value, BinaryWriter writer) {
					var str = value.Name.Text;
					writer.Write(str);
				}
				public static UriScheme Deserialize(BinaryReader reader) {
					var str = reader.ReadString();
					return new UriScheme(str);
				}
			}
		}
	}
}
