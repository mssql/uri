using System;
using System.Diagnostics;
using System.IO;
using UriLib.Support;
using UriLib.Validations;

namespace UriLib {
	/// <summary>
	/// Represents fragment part of <see cref="Url"/>.
	/// This structure is immutable and lazy.
	/// </summary>
	public partial struct UriFragment {
		public readonly StrSpan Text;

		public bool IsEmpty => this.Text.IsEmpty;

		private ValidationResult<UriFragment> validationResult;
		public ValidationResult<UriFragment> Validate() => this.validationResult ?? (this.validationResult = UriFragmentValidator.Validate(this));
		// if validation is null, we assume there is error
		public bool IsValid => !(this.Validate()?.IsError ?? true);

		public UriFragment(StrSpan text) {
			this.Text = text;
			this.validationResult = null;
		}

		public string ToUriString() {
			var txt = this.Text;

			var len = txt.Length;
			if (len < 1) { return null; }

			var result = txt.ToString();
			if ('#' == result[0]) {
				return result;
			}

			// TODO: can be optimized
			return "#" + result;
		}
		public override string ToString() => this.ToUriString();
		public string ToStringOrNull() => this.Text.ToStringOrNull();

		public override int GetHashCode() => this.Text.GetHashCode();
		public override bool Equals(object obj) {
			if (obj is null) {
				return (this.Text.Original is null);
			}
			else if (obj is UriFragment frag) {
				return this.Equals(frag);
			}
			else if (obj is StrSpan span) {
				return this.Equals(span);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}

			return false;
		}
	}

	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	partial struct UriFragment {
		private string DebuggerDisplay => this.ToUriString();
	}

	partial struct UriFragment {
		public static implicit operator UriFragment(StrSpan value) => new UriFragment(value);
		public static implicit operator UriFragment(string value) => new UriFragment(value);

		public static implicit operator StrSpan(UriFragment value) => value.Text;
		public static implicit operator string(UriFragment value) => value.ToUriString();
	}

	partial struct UriFragment : IEquatable<UriFragment> {
		public bool Equals(UriFragment other) {
			return this.Text.TextualyEquals(other.Text);
		}
	}

	partial struct UriFragment : IEquatable<StrSpan> {
		public bool Equals(StrSpan other) {
			return this.Text.TextualyEquals(other);
		}
	}

	partial struct UriFragment : IEquatable<string> {
		public bool Equals(string other) {
			return this.Text.TextualyEquals(other);
		}
	}

	partial struct UriFragment {
		public static class Serialization {
			public static void Serialize(ref UriFragment value, BinaryWriter writer) {
				writer.Write((byte)1);
				V1.Serialize(ref value, writer);
			}
			public static UriFragment Deserialize(BinaryReader reader) {
				var version = reader.ReadByte();
				if (0 == version) {
					// null
					return default(UriFragment);
				}
				if (1 == version) {
					return V1.Deserialize(reader);
				}

				throw new NotSupportedException($"The serialization viersion '{version}' is not supported.");
			}

			public static class V1 {
				public static void Serialize(ref UriFragment value, BinaryWriter writer) {
					var str = value.Text.Text;
					writer.Write(str);
				}
				public static UriFragment Deserialize(BinaryReader reader) {
					var str = reader.ReadString();
					return new UriFragment(str);
				}
			}
		}
	}
}
