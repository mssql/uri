using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UriLib.Support;
using UriLib.Validations;

namespace UriLib {
	public partial struct UriQueryPart {
		public readonly char Separator;
		public readonly StrSpan Key;
		public readonly bool HasAssignMark;
		public readonly StrSpan Value;
		private int? hashCode;

		public bool IsEmpty => AreEmptyValues(this.Separator, this.Key, this.Value, this.HasAssignMark);

		private ValidationResult<UriQueryPart> validationResult;
		public ValidationResult<UriQueryPart> Validate() => this.validationResult ?? (this.validationResult = UriQueryPartValidator.Validate(this));
		// if validation is null, we assume there is error
		public bool IsValid => !(this.Validate()?.IsError ?? true);

		public UriQueryPart(char separator, StrSpan key, StrSpan value) : this(separator, key, true, value) { }
		public UriQueryPart(char separator, StrSpan key, bool hasAssignMark, StrSpan value) {
			this.Separator = separator;
			this.Key = key;
			this.HasAssignMark = hasAssignMark;
			this.Value = value;
			this.hashCode = null;
			this.uriQueryPartString = null;
			this.validationResult = null;
		}

		public void Write(StringBuilder target) => this.Write(target, this.Separator);
		public void Write(StringBuilder target, char separator) {
			var key = this.Key;
			var value = this.Value;
			var hasAssignMark = this.HasAssignMark || !string.IsNullOrEmpty(value);

			if (hasAssignMark) {
				target.Append(separator).Append(key).Append('=').Append(value);
				return;
			}

			if (AreEmptyValues(separator, key, value, hasAssignMark)) {
				// nothing to write
				return;
			}

			target.Append(separator).Append(key);
			if (hasAssignMark) {
				target.Append('=').Append(value);
			}
		}

		public string ToUriQueryPartString() => this.ToUriQueryPartString(this.Separator);

		private string uriQueryPartString;
		public string ToUriQueryPartString(char separator) {
			var result = this.uriQueryPartString;
			if (!(result is null)) {
				// BUG: will return wrong result if 'separator' parameter differs from first character in 'result'
				return result;
			}

			var sb = new StringBuilder();
			this.Write(sb, separator);

			result = sb.ToString();
			this.uriQueryPartString = result;
			return result;
		}

		public static bool AreEmptyValues(char separator, string key, string value, bool hasAssignMark) {
			return ((char)0) == separator && string.IsNullOrEmpty(key) && string.IsNullOrEmpty(value) && !hasAssignMark;
		}

		public static UriQueryPart Parse(StrSpan source, out int lastIndex) {
			var ch = source.Original[source.StartIndex];
			if (!IsPartStartMark(ch)) {
				lastIndex = 0;
				return default(UriQueryPart);
			}

			var span = source.Skip(1);
			var result = ParseWithoutSeparator(span, ch, out int tmpLastIndex);
			lastIndex = tmpLastIndex + 1;
			return result;
		}
		public static UriQueryPart ParseWithoutSeparator(StrSpan source, char separator, out int lastIndex) {
			var part = source.Until(IsNotPartSeparator);
			lastIndex = part.Length;

			var key = part.Until(IsNotAssignMark);
			var rest = part.Skip(key.Length);
			var assignment = rest.Until(IsAssignMark);
			var lenAssignment = assignment.Length;
			if (lenAssignment < 1) {
				return new UriQueryPart(separator, key, false, default(StrSpan));
			}

			var value = rest.Skip(lenAssignment);
			return new UriQueryPart(separator, key, true, value);
		}

		public static bool IsSeparator(char ch) => ch == '?' || ch == '&' || ch == ';' || ch == '#';
		public static bool IsPartStartMark(char ch ) => ch == '?' || ch == '&' || ch == ';';
		public static bool IsPartSeparator(char ch) => ch == '?' || ch == '&' || ch == ';' || ch == '#';
		public static bool IsNotPartSeparator(char ch) => !IsPartSeparator(ch);
		public static bool IsAssignMark(char ch) => ch == '=';
		public static bool IsNotAssignMark(char ch) => !IsAssignMark(ch);
	}

	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	partial struct UriQueryPart {
		private string DebuggerDisplay => this.ToUriQueryPartString();
	}

	partial struct UriQueryPart {
		public override bool Equals(object obj) {
			if (obj is UriQueryPart part) {
				return this.Equals(part);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}
			else {
				return false;
			}
		}

		public override int GetHashCode() {
			var tmp = this.hashCode;
			if (tmp.HasValue) { return tmp.Value; }

			unchecked {
				var result = 17;
				result = result * 31 + this.Separator.GetHashCode();
				result = result * 31 + this.Key.GetHashCode();
				result = result * 31 + this.HasAssignMark.GetHashCode();
				result = result * 31 + this.Value.GetHashCode();

				this.hashCode = result;
				return result;
			}
		}
	}

	partial struct UriQueryPart : IEquatable<UriQueryPart> {
		public bool Equals(UriQueryPart other) {
			if (!this.Separator.Equals(other.Separator)) { return false; }
			if (!this.Key.TextualyEquals(other.Key)) { return false; }
			if (!this.HasAssignMark.Equals(other.HasAssignMark)) { return false; }
			if (!this.Value.TextualyEquals(other.Value)) { return false; }

			return true;
		}
	}

	partial struct UriQueryPart : IEquatable<string> {
		public bool Equals(string other) {
			var selfString = this.ToUriQueryPartString();
			return (0 == string.CompareOrdinal(selfString, other));
		}
	}
}
