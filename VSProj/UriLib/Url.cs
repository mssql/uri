using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using UriLib.Support;

namespace UriLib {
	/// <summary>
	/// Mutable structure of universal-resource-locator (subset of URI - universal-resource-identifier).
	/// </summary>
	/// <remarks>
	/// This is mutable structure composed from immutable members.
	/// </remarks>
	public partial struct Url {
	}

	// Fields of structure.
	partial struct Url {
		/// <summary>
		/// Scheme part of uri.
		/// </summary>
		public UriScheme Scheme;

		/// <summary>
		/// User-info part of uri.
		/// </summary>
		public UriUserInfo UserInfo;

		/// <summary>
		/// Host part of uri.
		/// </summary>
		public UriHost Host;

		/// <summary>
		/// Host port part of uri.
		/// </summary>
		public UriPort Port;

		/// <summary>
		/// Path part of uri.
		/// </summary>
		public UriPath Path;

		/// <summary>
		/// Query part of uri.
		/// </summary>
		public UriQuery Query;

		/// <summary>
		/// Fragment part of uri.
		/// </summary>
		public UriFragment Fragment;

		public void Reset() {
			this.Scheme = default(UriScheme);
			this.UserInfo = default(UriUserInfo);
			this.Host = default(UriHost);
			this.Port = default(UriPort);
			this.Path = default(UriPath);
			this.Query = default(UriQuery);
			this.Fragment = default(UriFragment);
		}

		public override int GetHashCode() {
			unchecked {
				var result = 17;
				result = result * 31 + this.Scheme.GetHashCode();
				result = result * 31 + this.UserInfo.GetHashCode();
				result = result * 31 + this.Host.GetHashCode();
				result = result * 31 + this.Port.GetHashCode();
				result = result * 31 + this.Path.GetHashCode();
				result = result * 31 + this.Query.GetHashCode();
				result = result * 31 + this.Fragment.GetHashCode();

				return result;
			}
		}
		public override bool Equals(object obj) {
			if (obj is Url uri) {
				return this.Equals(uri);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}

			return false;
		}

		public string ToUriString() {
			var sb = new StringBuilder();

			{
				var strScheme = this.Scheme.ToUriString();
				if (strScheme?.Length > 0) {
					sb.Append(strScheme);
					if (!strScheme.EndsWith("://")) {
						sb.Append("://");
					}
				}
			}

			{
				var strHost = this.Host.ToUriString();
				if (strHost?.Length > 0) {
					sb.Append(strHost);
				}
			}

			{
				var strPort = this.Port.ToUriString();
				if (strPort?.Length > 0) {
					if (!strPort.StartsWith(":")) {
						sb.Append(":");
					}
					sb.Append(strPort);
				}
			}

			{
				var strPath = this.Path.ToUriString();
				if (strPath?.Length > 0) {
					if (!strPath.StartsWith("/")) {
						sb.Append("/");
					}
					sb.Append(strPath);
				}
			}

			{
				var strQuery = this.Query.ToUriString();
				if (strQuery?.Length > 0) {
					if (!strQuery.StartsWith("?")) {
						sb.Append("?");
					}
					sb.Append(strQuery);
				}
			}

			{
				var strFragment = this.Fragment.ToUriString();
				if (strFragment?.Length > 0) {
					if (!strFragment.StartsWith("#")) {
						sb.Append("#");
					}
					sb.Append(strFragment);
				}
			}

			return sb.ToString();
		}
	}

	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	partial struct Url {
		private string DebuggerDisplay => $"{this.Scheme}{this.UserInfo}{this.Host}{this.Port}{this.Path}{this.Query}{this.Fragment}";
	}

	partial struct Url : IEquatable<Url> {
		public bool Equals(Url other) {
			if (!this.Scheme.Equals(other.Scheme)) { return false; }
			if (!this.UserInfo.Equals(other.UserInfo)) { return false; }
			if (!this.Host.Equals(other.Host)) { return false; }
			if (!this.Port.Equals(other.Port)) { return false; }
			if (!this.Path.Equals(other.Path)) { return false; }
			if (!this.Query.Equals(other.Query)) { return false; }
			if (!this.Fragment.Equals(other.Fragment)) { return false; }

			return true;
		}
	}

	partial struct Url : IEquatable<string> {
		public bool Equals(string other) {
			return (0 == string.CompareOrdinal(this.ToString(), other));
		}
	}

	// Computed members.
	partial struct Url {
		/// <summary>
		/// Computed property returning true if uri is empty.
		/// Othervise returns false.
		/// </summary>
		public bool IsEmpty {
			get {
				if (!this.Scheme.IsEmpty) { return false; }
				if (!this.Host.IsEmpty) { return false; }
				if (!this.Port.IsEmpty) { return false; }
				if (!this.Fragment.IsEmpty) { return false; }

				if (!this.UserInfo.IsEmpty) { return false; }
				if (!this.Path.IsEmpty) { return false; }
				if (!this.Query.IsEmpty) { return false; }

				return true;
			}
		}

		public bool IsValid() {
			return this.Scheme.IsValid
				&& this.UserInfo.IsValid
				&& this.Host.IsValid
				&& this.Port.IsValid
				&& this.Path.IsValid
				&& this.Query.IsValid
				&& this.Fragment.IsValid;
		}

		/// <summary>
		/// Count of parts in <see cref="Path"/>.
		/// </summary>
		public int PathPartsCount => this.Path.PartsCount;

		/// <summary>
		/// Count of parts in <see cref="Query"/>.
		/// </summary>
		public int QueryPartsCount => this.Query.PartsCount;
	}

	// Static factories.
	partial struct Url {
		/// <summary>
		/// Returns empty uri structure.
		/// </summary>
		public static Url Empty => default(Url);

		/// <summary>
		/// Will parse <paramref name="value"/> as <see cref="Url"/>.
		/// If parsing fails, exception is thrown.
		/// </summary>
		/// <param name="value">Value to parse.</param>
		/// <returns>Parsed uri.</returns>
		public static Url? Parse(string value) {
			TryParse(value, true, out var result, out _);
			return result;
		}

		/// <summary>
		/// Will try parse <paramref name="value"/> to <paramref name="result"/>.
		/// Returns true if parsing is complete.
		/// Othervise returns false.
		/// </summary>
		/// <param name="value">Value to parse.</param>
		/// <param name="result">Result of parsing. Will be empty if parsing fails.</param>
		/// <returns>
		/// True if parsing succeeds.
		/// Othervise false.
		/// </returns>
		public static bool TryParse(string value, out Url? result) => TryParse(value, false, out result, out _);

		/// <summary>
		/// Will try parse <paramref name="value"/> to <paramref name="result"/>.
		/// Returns true if parsing is complete.
		/// Othervise returns false and error is passed in <paramref name="error"/>.
		/// </summary>
		/// <param name="value">Value to parse.</param>
		/// <param name="result">Result of parsing. Will be empty if parsing fails.</param>
		/// <param name="error">Error during parsing. Will be null if parsing succeeds.</param>
		/// <returns>
		/// True if parsing succeeds.
		/// Othervise false.
		/// </returns>
		public static bool TryParse(string value, out Url? result, out Exception error) => TryParse(value, false, out result, out error);

		/// <summary>
		/// Will try parse <paramref name="value"/> to <paramref name="result"/>.
		/// Returns true if parsing is complete.
		/// Othervise returns false and error is passed in <paramref name="error"/>.
		/// </summary>
		/// <param name="value">Value to parse.</param>
		/// <param name="throwsOnError">If true, the exception will be thrown instead of passing exception to <paramref name="error"/> and returning false.</param>
		/// <param name="result">Result of parsing. Will be empty if parsing fails.</param>
		/// <param name="error">Error during parsing. Will be null if parsing succeeds.</param>
		/// <returns>
		/// True if parsing succeeds.
		/// Othervise false.
		/// </returns>
		private static bool TryParse(string value, bool throwsOnError, out Url? result, out Exception error) {
			if (value is null) {
				result = null;
				error = null;
				return true;
			}

			var okValue = value?.Trim();
			var okResult = new Url();
			if (string.IsNullOrEmpty(okValue)) {
				result = okResult;
				error = null;
				return true;
			}

			const string SCHEME_END = "://";

			var parseIndex = 0;
			var ndxSchemeEnd = okValue.IndexOf(SCHEME_END, parseIndex);
			if (ndxSchemeEnd >= 0) {
				okResult.Scheme = new StrSpan(okValue, 0, ndxSchemeEnd + SCHEME_END.Length);
				parseIndex = ndxSchemeEnd + SCHEME_END.Length;
			}

			var ndxUserInfoTerminator = okValue.IndexOf('@', parseIndex);
			if (ndxUserInfoTerminator >= parseIndex) {
				var ndxPasswordSeparator = okValue.IndexOf(':', parseIndex);
				if (ndxPasswordSeparator < 0) {
					var len = ndxUserInfoTerminator - parseIndex;
					var nameSpan = new StrSpan(okValue, parseIndex, len);
					okResult.UserInfo = new UriUserInfo(nameSpan);
				}
				else {
					var lenName = ndxPasswordSeparator - parseIndex;
					var nameSpan = new StrSpan(okValue, parseIndex, lenName);
					var lenPwd = ndxUserInfoTerminator - ndxPasswordSeparator;
					var pwdSpan = new StrSpan(okValue, ndxPasswordSeparator + 1, lenPwd - 1);
					okResult.UserInfo = new UriUserInfo(nameSpan, pwdSpan);
				}
				parseIndex = ndxUserInfoTerminator + 1;
			}

			var ndxDotDot = okValue.IndexOf(':', parseIndex);
			var ndxSlash = okValue.IndexOf('/', parseIndex);
			var ndxQuery = okValue.IndexOf('?', parseIndex);
			var ndxHash = okValue.IndexOf('#', parseIndex);

			int? NextIndexNullable2(int? ndx0, int? ndx1) {
				if (!ndx0.HasValue) { return ndx1; }
				if (!ndx1.HasValue) { return ndx0; }

				if (ndx0 < ndx1) { return ndx0; }
				else { return ndx1; }
			}
			int? NextIndexNullable3(int? ndx0, int? ndx1, int? ndx2) {
				var ndxTmp = NextIndexNullable2(ndx0, ndx1);
				return NextIndexNullable2(ndxTmp, ndx2);
			}

			int? NextIndex2(int ndx0, int ndx1) {
				var okNdx0 = ndx0 < 0 ? (int?)null : ndx0;
				var okNdx1 = ndx1 < 0 ? (int?)null : ndx1;
				return NextIndexNullable2(okNdx0, okNdx1);
			}
			int? NextIndex3(int ndx0, int ndx1, int ndx2) {
				var okNdx0 = ndx0 < 0 ? (int?)null : ndx0;
				var okNdx1 = ndx1 < 0 ? (int?)null : ndx1;
				var okNdx2 = ndx2 < 0 ? (int?)null : ndx2;
				return NextIndexNullable3(okNdx0, okNdx1, okNdx2);
			}

			if (ndxHash >= parseIndex) {
				var len = okValue.Length - ndxHash;
				okResult.Fragment = new StrSpan(okValue, ndxHash, len);
			}
			if (ndxQuery >= parseIndex) {
				int len = ndxHash < 0 ? okValue.Length - ndxQuery : ndxHash - ndxQuery;
				var span = new StrSpan(okValue, ndxQuery, len);
				okResult.Query = new UriQuery(span);
			}
			if (ndxSlash >= parseIndex) {
				var ndxNext = NextIndex2(ndxQuery, ndxHash) ?? okValue.Length;
				var len = ndxNext - ndxSlash;
				var span = new StrSpan(okValue, ndxSlash, len);
				okResult.Path = new UriPath(span);
			}
			if (ndxDotDot >= parseIndex) {
				var ndxNext = NextIndex3(ndxSlash, ndxQuery, ndxHash) ?? okValue.Length;
				var len = ndxNext - ndxDotDot;
				okResult.Port = new StrSpan(okValue, ndxDotDot + 1, len - 1);
			}

			var ndxEndOfHost = okValue.Length;
			if (ndxDotDot >= parseIndex && ndxEndOfHost > ndxDotDot) { ndxEndOfHost = ndxDotDot; }
			if (ndxSlash >= parseIndex && ndxEndOfHost > ndxSlash) { ndxEndOfHost = ndxSlash; }
			if (ndxQuery >= parseIndex && ndxEndOfHost > ndxQuery) { ndxEndOfHost = ndxQuery; }
			if (ndxHash >= parseIndex && ndxEndOfHost > ndxHash) { ndxEndOfHost = ndxHash; }

			var hostLen = ndxEndOfHost - parseIndex;
			okResult.Host = new StrSpan(okValue, parseIndex, hostLen);

			// TODO: add exception handling
			result = okResult;
			error = null;
			return true;
		}
	}

	// Fluent edit methods.
	partial struct Url {
		#region Scheme
		/// <summary>
		/// Returns new uri (original is not modified) with <see cref="Scheme"/> assigned from <paramref name="value"/>.
		/// </summary>
		/// <param name="value">New value for <see cref="Scheme"/>.</param>
		public Url WithScheme(string value) {
			var result = this;
			result.Scheme = value;
			return result;
		}

		/// <summary>
		/// Returns new uri (original is not modified) without <see cref="Scheme"/> value.
		/// </summary>
		public Url WithoutScheme() => WithScheme(null);
		#endregion Scheme

		#region UserInfo
		public Url WithUserInfo(UriUserInfo value) {
			var result = this;
			result.UserInfo = value;
			return result;
		}
		public Url WithoutUserInfo() => WithUserInfo(default(UriUserInfo));

		public Url WithUserInfo(string userName, string userPassword) {
			var userInfo = this.UserInfo.WithNameAndPassword(userName, userPassword);
			return this.WithUserInfo(userInfo);
		}
		#endregion UserInfo

		#region UserName
		/// <summary>
		/// Returns new uri (original is not modified) with <see cref="UserName"/> assigned from <paramref name="value"/>.
		/// </summary>
		/// <param name="value">New value for <see cref="UserName"/>.</param>
		public Url WithUserName(string value) {
			var userInfo = this.UserInfo.WithName(value);
			return this.WithUserInfo(userInfo);
		}

		/// <summary>
		/// Returns new uri (original is not modified) without <see cref="UserName"/> value.
		/// </summary>
		public Url WithoutUserName() {
			var userInfo = this.UserInfo.WithoutName();
			return this.WithUserInfo(userInfo);
		}
		#endregion UserName

		#region UserPassword
		public Url WithUserPassword(string value) {
			var userInfo = this.UserInfo.WithPassword(value);
			return this.WithUserInfo(userInfo);
		}
		public Url WithoutUserPassword() {
			var userInfo = this.UserInfo.WithoutPassword();
			return this.WithUserInfo(userInfo);
		}
		#endregion UserPassword

		#region Host
		public Url WithHost(string value) {
			var result = this;
			result.Host = value;
			return result;
		}
		public Url WithoutHost() => WithHost(null);
		#endregion Host

		#region Port
		public Url WithPort(int? value) {
			var result = this;
			result.Port = value;
			return result;
		}
		public Url WithoutPort() => WithPort(null);
		#endregion Port

		#region Path
		public Url WithPath(UriPath value) {
			var result = this;
			result.Path = value;
			return result;
		}
		public Url WithPath(string value) => this.WithPath(new UriPath(value));
		public Url WithoutPath() => this.WithPath(default(UriPath));
		#endregion Path

		#region Query
		public Url WithQuery(UriQuery value) {
			var result = this;
			result.Query = value;
			return result;
		}
		public Url WithQuery(string value) => WithQuery(new UriQuery(value));
		public Url WithoutQuery() => WithQuery(default(UriQuery));
		#endregion Query

		#region Fragment
		public Url WithFragment(string fragment) {
			var result = this;
			result.Fragment = fragment;
			return result;
		}
		public Url WithoutFragment() => WithFragment(null);
		#endregion Fragment
	}

	partial struct Url /* : IBinarySerialize */ {
		public void Read(BinaryReader r) {
			var version = r.ReadByte();
			if (0 == version) {
				// null
				this.Reset();
			}
			else if (1 == version) {
				this.ReadVersion1(r);
			}
			else {
				throw new NotSupportedException($"The serialization version '{version}' is not supported.");
			}
		}

		private void ReadVersion1(BinaryReader r) {
			this.Scheme = UriScheme.Serialization.Deserialize(r);
			this.UserInfo = UriUserInfo.Serialization.Deserialize(r);
			this.Host = UriHost.Serialization.Deserialize(r);
			this.Port = UriPort.Deserialize(r);
			this.Path = UriPath.Serialization.Deserialize(r);
			this.Query = UriQuery.Deserialize(r);
			this.Fragment = UriFragment.Serialization.Deserialize(r);
		}

		public void Write(BinaryWriter w) {
			w.Write((byte)1);
			this.WriteVersion1(w);
		}

		private void WriteVersion1(BinaryWriter w) {
			UriScheme.Serialization.Serialize(ref this.Scheme, w);
			UriUserInfo.Serialization.Serialize(ref this.UserInfo, w);
			UriHost.Serialization.Serialize(ref this.Host, w);
			UriPort.Serialize(ref this.Port, w);
			UriPath.Serialization.Serialize(ref this.Path, w);
			UriQuery.Serialize(ref this.Query, w);
			UriFragment.Serialization.Serialize(ref this.Fragment, w);
		}
	}
}
