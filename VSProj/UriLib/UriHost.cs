using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UriLib.Support;
using UriLib.Validations;

namespace UriLib {
	/// <summary>
	/// Represents host part of <see cref="Url"/>.
	/// This structure is immutable and lazy.
	/// </summary>
	public partial struct UriHost {
		public readonly StrSpan Text;

		public bool IsEmpty => this.Text.IsEmpty;

		private ValidationResult<UriHost> validationResult;
		public ValidationResult<UriHost> Validate() => this.validationResult ?? (this.validationResult = UriHostValidator.Validate(this));
		// if validation is null, we assume there is error
		public bool IsValid => !(this.Validate()?.IsError ?? true);

		public UriHost(StrSpan text) {
			this.Text = text;
			this.validationResult = null;
		}

		public string ToUriString() => this.Text.ToStringOrNull();
		public override string ToString() => this.ToUriString();

		public override int GetHashCode() => this.Text.GetHashCode();
		public override bool Equals(object obj) {
			if (obj is null) {
				return (this.Text.Original is null);
			}
			else if (obj is UriHost host) {
				return this.Equals(host);
			}
			else if (obj is StrSpan span) {
				return this.Equals(span);
			}
			else if (obj is string str) {
				return this.Equals(str);
			}

			return false;
		}
	}

	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	partial struct UriHost {
		private string DebuggerDisplay => this.ToUriString();
	}

	partial struct UriHost {
		public static implicit operator UriHost(StrSpan value) => new UriHost(value);
		public static implicit operator UriHost(string value) => new UriHost(value);

		public static implicit operator StrSpan(UriHost value) => value.Text;
		public static implicit operator string(UriHost value) => value.ToUriString();
	}

	partial struct UriHost : IEquatable<UriHost> {
		public bool Equals(UriHost other) {
			return this.Text.TextualyEquals(other.Text);
		}
	}

	partial struct UriHost : IEquatable<StrSpan> {
		public bool Equals(StrSpan other) {
			return this.Text.TextualyEquals(other);
		}
	}

	partial struct UriHost : IEquatable<string> {
		public bool Equals(string other) {
			return this.Text.TextualyEquals(other);
		}
	}

	partial struct UriHost {
		public static class Serialization {
			public static void Serialize(ref UriHost value, BinaryWriter writer) {
				writer.Write((byte)1);
				V1.Serialize(ref value, writer);
			}
			public static UriHost Deserialize(BinaryReader reader) {
				var version = reader.ReadByte();
				if (0 == version) {
					// null
					return default(UriHost);
				}
				if (1 == version) {
					return V1.Deserialize(reader);
				}

				throw new NotSupportedException($"The serialization viersion '{version}' is not supported.");
			}

			public static class V1 {
				public static void Serialize(ref UriHost value, BinaryWriter writer) {
					var str = value.Text.Text;
					writer.Write(str);
				}
				public static UriHost Deserialize(BinaryReader reader) {
					var str = reader.ReadString();
					return new UriHost(str);
				}
			}
		}
	}
}
