using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UriLib.Support;
using UriLib.Validations;

namespace UriLib {
	/// <summary>
	/// Represents path part of <see cref="Url"/>.
	/// This structure is immutable and lazy.
	/// </summary>
	public partial struct UriPath {
		private StrSpan text;
		public StrSpan Text {
			get {
				var result = this.text;
				var strResult = result.Original;
				if (!(strResult is null)) { return result; }

				var lst = this.parts;
				if (lst is null) { return null; }

				this.text = result = ComposeText(lst);
				return result;
			}
		}

		private IReadOnlyList<StrSpan> parts;
		public IReadOnlyList<StrSpan> Parts {
			get {
				var result = this.parts;
				if (!(result is null)) { return result; }

				var str = this.text;
				if (string.IsNullOrEmpty(str)) { return null; }

				var arr = SplitToParts(str);
				this.parts = result = new System.Collections.ObjectModel.ReadOnlyCollection<StrSpan>(arr);
				return result;
			}
		}

		public int PartsCount => this.Parts?.Count ?? 0;

		public bool IsEmpty => string.IsNullOrEmpty(this.text) && 1 > (this.parts?.Count ?? 0);

		private ValidationResult<UriPath> validationResult;
		public ValidationResult<UriPath> Validate() => this.validationResult ?? (this.validationResult = UriPathValidator.Validate(this));
		// if validation is null, we assume there is error
		public bool IsValid => !(this.Validate()?.IsError ?? true);

		public UriPath(string text) {
			this.text = text;
			this.parts = null;
			this.validationResult = null;
		}
		public UriPath(string[] parts) {
			this.text = null;
			this.parts = ToClonedParts(parts);
			this.validationResult = null;
		}
		public UriPath(StrSpan[] parts) : this() {
			this.text = null;

			if (!(parts is null) && parts.Length > 0) {
				var clonedParts = (StrSpan[])parts?.Clone();
				this.parts = new System.Collections.ObjectModel.ReadOnlyCollection<StrSpan>(clonedParts);
			}
			else {
				this.parts = null;
			}
		}

		public string ToUriString() => this.Text;

		private static StrSpan[] ToClonedParts(string[] parts) {
			if (parts is null || parts.Length < 1) {
				return null;
			}

			var result = new StrSpan[parts.Length];
			for (var i = 0; i < result.Length; i++) {
				result[i] = parts[i];
			}

			return result;
		}

		public static string ComposeText(IReadOnlyList<StrSpan> parts) {
			if (parts is null || parts.Count < 1) {
				return null;
			}

			var result = string.Join("/", parts);
			return result;
		}

		public static StrSpan[] SplitToParts(string path) => StrSpan.Split(path, '/');
	}

	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	partial struct UriPath {
		private string DebuggerDisplay => this.ToUriString();
	}

	partial struct UriPath {
		public static class Serialization {
			public static void Serialize(ref UriPath value, BinaryWriter writer) {
				writer.Write((byte)1);
				V1.Serialize(ref value, writer);
			}
			public static UriPath Deserialize(BinaryReader reader) {
				var version = reader.ReadByte();
				if (0 == version) {
					// null
					return default(UriPath);
				}
				if (1 == version) {
					return V1.Deserialize(reader);
				}

				throw new NotSupportedException($"The serialization viersion '{version}' is not supported.");
			}

			public static class V1 {
				public static void Serialize(ref UriPath value, BinaryWriter writer) {
					// TODO: can be optimized for case where parts are available
					var str = value.Text.Text;
					writer.Write(str);
				}
				public static UriPath Deserialize(BinaryReader reader) {
					var str = reader.ReadString();
					return new UriPath(str);
				}
			}
		}
	}
}
