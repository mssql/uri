﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using UriLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClrUriLib_TEST {
	[TestClass]
	public class ClrUri_Tests {

		private ClrUri Reserialize(ClrUri value) {
			using (var mstrm = new MemoryStream()) {
				var writer = new BinaryWriter(mstrm);
				value.Write(writer);

				mstrm.Position = 0;
				var reader = new BinaryReader(mstrm);
				var result = new ClrUri();
				result.Read(reader);
				return result;
			}
		}

		[TestMethod]
		public void SerializationWorks() {
			void AssertReserializedUri(ClrUri value) {
				var srcValue = value.ParsedValue;
				{
					var scheme = srcValue.Scheme.ToUriString();
					var userInfo = srcValue.UserInfo.ToUriString();
					var host = srcValue.Host.ToUriString();
					var port = srcValue.Port.ToUriString();
					var path = srcValue.Path.ToUriString();
					var query = srcValue.Query.Text;
					var fragment = srcValue.Fragment.ToUriString();
				}

				var copy = this.Reserialize(value);
				var copValue = copy.ParsedValue;

				srcValue.Equals(copValue);
				copValue.Equals(srcValue);
			}
			void AssertReserializedUriString(string strValue) {
				var uri = new ClrUri(strValue);
				AssertReserializedUri(uri);
			}

			AssertReserializedUri(new ClrUri());
			AssertReserializedUriString("http");
			AssertReserializedUriString("http://");
			AssertReserializedUriString("http://google.com");
			AssertReserializedUriString("http://google.com/search");
			AssertReserializedUriString("http://google.com/search/images");
			AssertReserializedUriString("http://google.com/search/images?q=a;f=123&q=78#my-fragment");
			AssertReserializedUriString("http://user:pwd@google.com/search/images?q=a;f=123&q=78#my-fragment");
			AssertReserializedUriString("http://user:pwd@google.com:443/search/images?q=a;f=123&q=78#my-fragment");
		}

		[TestMethod]
		public void Text_Works() {
			var url = "https://www.google.com";
			var orig = ClrUri.Parse((SqlString)url);
			Assert.AreEqual(url, orig.Text);
		}

        [TestMethod]
        public void GetSchemeWorks()
        {
            var result = new ClrUri("https://www.google.cz/");
            Assert.AreEqual("https://", result.Scheme);

        }

        [TestMethod]
		public void EmptyUriIsNull() {
			Assert.IsTrue(default(ClrUri).IsNull);
		}

		[TestMethod]
		public void CanCreateUriWithNullString() {
			var emptyUri = new ClrUri((string)null);
			Assert.IsTrue(emptyUri.IsNull);
		}

		[TestMethod]
		public void InvalidUriIsInvalid() {
			var invalidUri = new ClrUri(":)");
			Assert.IsTrue(invalidUri.IsInvalid);
			Assert.IsFalse(invalidUri.IsValid);
			Assert.IsFalse(invalidUri.IsNull);
		}

		[TestMethod]
		public void ValidUriIsValid() {
			var validUri = new ClrUri("https://www.google.cz/");
			Assert.IsTrue(validUri.IsValid);
			Assert.IsFalse(validUri.IsNull);
			Assert.IsFalse(validUri.IsInvalid);
		}

		void AssertQueryValue(ClrUri uri, string key, string expectedValue) {
			var actualValue = uri.GetQueryValue(key);
			Assert.AreEqual(expectedValue, actualValue);
		}

		[TestMethod]
		public void GetQueryValueReturnsValidValue() {
			var validUri = new ClrUri("https://www.google.cz/search?newwindow=1&rlz=1C1CHBF_enCZ766CZ766&ei=oEP_WYqSKZLWkwXe87_IBQ&q=uri&oq=uri&gs_l=psy-ab.3..35i39k1j0i67k1l4j0j0i20i263k1j0i67k1l3.221934.222319.0.222532.2.2.0.0.0.0.193.285.1j1.2.0....0...1.1.64.psy-ab..0.2.284....0.N-ZK76_hiAY");
			var newwindowValue = validUri.GetQueryValue("newwindow");
			var rlzValue = validUri.GetQueryValue("rlz");
			var qValue = validUri.GetQueryValue("q");
			Assert.AreEqual("1", newwindowValue);
			Assert.AreEqual("1C1CHBF_enCZ766CZ766", rlzValue);
			Assert.AreEqual("uri", qValue);

		}

		[TestMethod]
		public void GetQueryValueReturnsValidValue_2() {
			var subject = new ClrUri("https://myuser:mypassword@google.com/search/images?some=arguments&which=are&totaly=nonsense&1=2;or=something;else&or-not");
			AssertQueryValue(subject, "some", "arguments");
			AssertQueryValue(subject, "which", "are");
			AssertQueryValue(subject, "totaly", "nonsense");
			AssertQueryValue(subject, "1", "2");
			AssertQueryValue(subject, "or", "something");
			AssertQueryValue(subject, "else", null);
			AssertQueryValue(subject, "or-not", null);
		}

		private void AssertNullity(ClrUri subject) {
			Assert.IsTrue(subject.IsNull);
			Assert.IsTrue(subject.Validate());
			Assert.IsTrue(subject.IsValid);
			Assert.IsFalse(subject.IsInvalid);
		}
		private void AssertUriEquality(UriLib.Url? expected, ClrUri subject) {
			if (expected is null) {
				AssertNullity(subject);
				return;
			}

			var ok = expected.Value;

			Assert.AreEqual(ok.Fragment.ToUriString(), subject.Fragment);
			Assert.AreEqual(ok.Host.ToUriString(), subject.Host);
			Assert.AreEqual(ok.Path.Text.Text, subject.Path);
			Assert.AreEqual(ok.Query.Text.Text, subject.Query);
			XAssert.AreOrdinalEqual(ok.Scheme.ToUriString(), subject.Scheme);
			XAssert.AreOrdinalEqual(ok.Scheme.Name, subject.SchemeName);
			Assert.AreEqual(ok.UserInfo.ToUriString(), subject.UserInfo);

			//Assert.AreEqual(expected.GetAbsolutePath(), subject.AbsolutePath);
			//Assert.AreEqual(expected.GetAbsoluteUri(), subject.AbsoluteUri);
			//Assert.AreEqual(expected.GetAuthority(), subject.Authority);
			//Assert.AreEqual(expected.GetDnsSafeHost(), subject.DnsSafeHost);
			//Assert.AreEqual(expected.GetFragment(), subject.Fragment);
			//Assert.AreEqual(expected.GetHost(), subject.Host);
			//Assert.AreEqual(expected.GetHostNameType().ToString(), subject.HostNameType);
			//Assert.AreEqual(expected.GetIdnHost(), subject.IdnHost);
			//Assert.AreEqual(expected.GetIsAbsoluteUri(), subject.IsAbsoluteUri);
			//Assert.AreEqual(expected.GetIsDefaultPort(), subject.IsDefaultPort);
			//Assert.AreEqual(expected.GetIsFile(), subject.IsFile);
			//Assert.AreEqual(expected.GetIsLoopback(), subject.IsLoopback);
			//Assert.AreEqual(expected.GetIsUnc(), subject.IsUnc);
			//Assert.AreEqual(expected.GetLocalPath(), subject.LocalPath);
			//Assert.AreEqual(expected.OriginalString, subject.Original);
			//Assert.AreEqual(expected.GetPathAndQuery(), subject.PathAndQuery);
			//Assert.AreEqual(expected.GetPort(), subject.Port);
			//Assert.AreEqual(expected.GetQuery(), subject.Query);
			//Assert.AreEqual(expected.GetScheme(), subject.Scheme);
			//Assert.AreEqual(expected.GetUserEscaped(), subject.UserEscaped);
			//Assert.AreEqual(expected.GetUserInfo(), subject.UserInfo);
		}
		//private void AssertUriEquality(string uriString) {
		//	var uri = uriString is null ? null : new Uri(uriString, UriKind.RelativeOrAbsolute);
		//	var clrUri = new ClrUri(uriString);

		//	AssertUriEquality(uri, clrUri);
		//}

		[TestMethod]
		public void Equality_NULL_Works() {
			AssertUriEquality(null, ClrUri.Null);
		}

		[TestMethod]
		public void Equality_StringEmpty_Works() {
			AssertUriEquality(default(UriLib.Url), ClrUri.Empty);
		}

		[TestMethod]
		public void Equality_GoogleCom_Works() {
			var expected = new UriLib.Url {
				Host = "google.com"
			};
			var actual = new ClrUri("google.com");

			AssertUriEquality(expected, actual);
		}

		[TestMethod]
		public void Equality_HttpsUserPasswordGoogleComBlaBlaBla_Works() {
			var expected = new UriLib.Url {
				Scheme = "https://",
				UserInfo = new UriUserInfo("myuser", "mypassword"),
				Host = "google.com",
				Path = new UriPath("/search/images"),
				Query = new UriQuery(
					new UriQueryPart('?', "some", "arguments"),
					new UriQueryPart('&', "which", "are"),
					new UriQueryPart('&', "totaly", "nonsense"),
					new UriQueryPart('&', "1", "2"),
					new UriQueryPart(';', "or", "something"),
					new UriQueryPart(';', "else", false, null),
					new UriQueryPart('&', "or-not", false, null)
					)
			};

			var actual = new ClrUri("https://myuser:mypassword@google.com/search/images?some=arguments&which=are&totaly=nonsense&1=2;or=something;else&or-not");

			AssertUriEquality(expected, actual);
		}

        [TestMethod]
        public void GetQueryPartsWorks_HttpsUserPasswordGoogleComBlaBlaBla()
        {
            var subject = new ClrUri("https://myuser:mypassword@google.com/search/images?some=arguments&which=are&totaly=nonsense&1=2;or=something;else&or-not");
            var actualParts = subject.ParsedValue.Query.Parts.Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToArray();
            var expectedParts = new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("some", "arguments"),
                new KeyValuePair<string, string>("which", "are"),
                new KeyValuePair<string, string>("totaly", "nonsense"),
                new KeyValuePair<string, string>("1", "2"),
                new KeyValuePair<string, string>("or", "something"),
                new KeyValuePair<string, string>("else", null),
                new KeyValuePair<string, string>("or-not", null)
            };

            CollectionAssert.AreEquivalent(expectedParts, actualParts);
        }

        [TestMethod]
        public void GetQueryValueLastWorks_NULLUri()
        {
            var subject = new ClrUri(null);
            Assert.AreEqual(null, subject.GetQueryValueLast("test"));
        }

        [TestMethod]
        public void GetQueryValueLastWorks_ValidUriNULLParameter()             
        {
            var subject = new ClrUri("google.com");
            Assert.AreEqual(null, subject.GetQueryValueLast(null));
        }

        [TestMethod]
        public void GetQueryValueLastWorks_ValidUriOneOccurence()
        {
            var subject = new ClrUri("https://www.google.cz/search?q=snow+station&as_eq=clap+berry");
            Assert.AreEqual("snow+station", subject.GetQueryValueLast("q"));
        }

        [TestMethod]
        public void GetQueryValueLast_ValidUriMultipleOccurences()
        {
            var subject = new ClrUri("https://www.google.cz/search?q=snow+station&q=clap+berry");
            Assert.AreEqual("clap+berry", subject.GetQueryValueLast("q"));
        }

        [TestMethod]
        public void GetQueryValuesAsJSONWorks_MultipleQueryValues()
        {
            var subject = new ClrUri("https://www.google.cz/search?q=snow+station&q=clap+berry");
            Assert.AreEqual("[\"snow+station\",\"clap+berry\"]", subject.GetQueryValuesAsJSON("q"));
        }

        [TestMethod]
        public void GetPathPartWorks_ValidURI()
        {
            var subject = new ClrUri("http://ads.ad4game.com/www/delivery/dck.php?offerid=2013&zoneid=62938&subid2=388006602142&affid=546563");
            Assert.AreEqual("www", subject.GetPathPart(1));
            Assert.AreEqual("delivery", subject.GetPathPart(2));
            Assert.AreEqual("dck.php", subject.GetPathPart(3));
        }

        [TestMethod]
        public void GetPathPartWorks_InvalidURI()
        {
            var subject = new ClrUri(":)");
            Assert.AreEqual(null, subject.GetPathPart(0));
        }

        [TestMethod]
        public void GetPathPartWorks_NULLUri()
        {
            var subject = new ClrUri(null);
            Assert.AreEqual(null, subject.GetPathPart(1));
            Assert.AreEqual(null, subject.GetPathPart(0));
        }

        [TestMethod]
        public void GetQueryPathPartWorks_ValidURIInvalidIndex()
        {
            var subject = new ClrUri("http://www.gardners-eshop.cz/workshopy/");
            Assert.AreEqual(null, subject.GetPathPart(3));
        }

        [TestMethod]
        public void GetFirstNonBlankPathPartWorks_ValidURI()
        {
            var subject = new ClrUri("http://ads.ad4game.com/www/delivery/dck.php?offerid=2013&zoneid=62938&subid2=388006602142&affid=546563");
            Assert.AreEqual("www", subject.GetFirstNonBlankPathPart());           
        }

        [TestMethod]
        public void GetFirstNonBlankPathPartWorks_IncompleteURI()
        {
            var subject = new ClrUri("/www/delivery/dck.php?offerid=2013&zoneid=62938&subid2=388006602142&affid=546563");
            Assert.AreEqual("www", subject.GetFirstNonBlankPathPart());
        }

        [TestMethod]
        public void GetPathsPartsWorks_ValidURI()
        {
            var subject = new ClrUri("http://www.gardners-eshop.cz/workshopy/");
            var expectedResult = new KeyValuePair<int, string>[]
            {
                new KeyValuePair<int, string>(0, ""),
                new KeyValuePair<int, string>(1, "workshopy"),
                new KeyValuePair<int, string>(2, "")
            };           

            CollectionAssert.AreEquivalent(expectedResult, subject.GetPartParts());
        }

        [TestMethod]
        public void GetPathPartsWorks_InvalidURI()
        {
            var subject = new ClrUri(":)");
            Assert.AreEqual(null, subject.GetPartParts());
        }

        [TestMethod]
        public void GetPathPartsWorks_NULLUri()
        {
            var subject = new ClrUri(null);
            Assert.AreEqual(null, subject.GetPartParts());
        }

        [TestMethod]
        public void GetPathSubPartWorks_ValidURIValidIndexValidLength()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455");
            Assert.AreEqual("profile/keys", subject.GetPathSubpart(1, 2));
            Assert.AreEqual("keys", subject.GetPathSubpart(2, 1));
            Assert.AreEqual("profile/keys/1579455", subject.GetPathSubpart(1, 3));
            Assert.AreEqual("/profile/keys/1579455", subject.GetPathSubpart(0, 4));
        }

        [TestMethod]
        public void GetPathSubpartWorks_ValidURIInvalidIndexInvalidLength()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455");
            Assert.AreEqual(null, subject.GetPathSubpart(10, 2));
            Assert.AreEqual(null, subject.GetPathSubpart(-2, 1));
            Assert.AreEqual(null, subject.GetPathSubpart(10, 0));
            Assert.AreEqual(null, subject.GetPathSubpart(-2, -2));
            Assert.AreEqual(null, subject.GetPathSubpart(0, -2));
            Assert.AreEqual(null, subject.GetPathSubpart(2, 10));
        }

        [TestMethod]
        public void GetPathSubpartWorks_InvalidURI()
        {
            var subject = new ClrUri(":)");
            Assert.AreEqual(null, subject.GetPathSubpart(1, 2));
            Assert.AreEqual(null, subject.GetPathSubpart(2, 1));
            Assert.AreEqual(null, subject.GetPathSubpart(10, 2));
            Assert.AreEqual(null, subject.GetPathSubpart(-2, 1));
            Assert.AreEqual(null, subject.GetPathSubpart(10, 0));
            Assert.AreEqual(null, subject.GetPathSubpart(-2, -2));
            Assert.AreEqual(null, subject.GetPathSubpart(0, -2));
            Assert.AreEqual(null, subject.GetPathSubpart(2, 10));
        }

        [TestMethod]
        public void GetPathFirstIndexOfPartWorks_ValidURIValidPart()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455");
            Assert.AreEqual(1, subject.GetPathFirstIndexOfPart("profile"));
            Assert.AreEqual(2, subject.GetPathFirstIndexOfPart("keys"));
            Assert.AreEqual(3, subject.GetPathFirstIndexOfPart("1579455"));
        }

        [TestMethod]
        public void GetPathFirstIndexOfPartWorks_ValidURIInvalidPart()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455");
            Assert.AreEqual(null, subject.GetPathFirstIndexOfPart("test"));
            Assert.AreEqual(null, subject.GetPathFirstIndexOfPart(null));
        }

        [TestMethod]
        public void GetPathFirstIndexOfPartWorks_InvalidURI()
        {
            var subject = new ClrUri(":)");
            Assert.AreEqual(null, subject.GetPathFirstIndexOfPart(null));
            Assert.AreEqual(null, subject.GetPathFirstIndexOfPart("test"));
        }

        [TestMethod]
        public void GetPathFirstIndexOfPartWorks_NULLURI()
        {
            var subject = new ClrUri(null);
            Assert.AreEqual(null, subject.GetPathFirstIndexOfPart(null));
            Assert.AreEqual(null, subject.GetPathFirstIndexOfPart("test"));
        }

        [TestMethod]
        public void GetPathFirstIndexOfPartWorks_ValidURIMultipleParts()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455/keys");
            Assert.AreEqual(2, subject.GetPathFirstIndexOfPart("keys"));
        }


        [TestMethod]
        public void GetPathLastIndexOfPartWorks_ValidURIValidPart()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455");
            Assert.AreEqual(1, subject.GetPathLastIndexOfPart("profile"));
            Assert.AreEqual(2, subject.GetPathLastIndexOfPart("keys"));
            Assert.AreEqual(3, subject.GetPathLastIndexOfPart("1579455"));
        }

        [TestMethod]
        public void GetPathLastIndexOfPartWorks_ValidURIInvalidPart()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455");
            Assert.AreEqual(null, subject.GetPathLastIndexOfPart("test"));
            Assert.AreEqual(null, subject.GetPathLastIndexOfPart(null));
        }

        [TestMethod]
        public void GetPathLastIndexOfPartWorks_InvalidURI()
        {
            var subject = new ClrUri(":)");
            Assert.AreEqual(null, subject.GetPathLastIndexOfPart(null));
            Assert.AreEqual(null, subject.GetPathLastIndexOfPart("test"));
        }

        [TestMethod]
        public void GetPathLastIndexOfPartWorks_NULLURI()
        {
            var subject = new ClrUri(null);
            Assert.AreEqual(null, subject.GetPathLastIndexOfPart(null));
            Assert.AreEqual(null, subject.GetPathLastIndexOfPart("test"));
        }

        [TestMethod]
        public void GetPathLastIndexOfPartWorks_ValidURIMultipleParts()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455/keys");
            Assert.AreEqual(4, subject.GetPathLastIndexOfPart("keys"));
        }

        [TestMethod]
        public void GetPathGetPathPartIndicesWorks_ValidURI()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455/keys");
            var expectedResult = new int[2];
            expectedResult[0] = 2;
            expectedResult[1] = 4;
            CollectionAssert.AreEquivalent(expectedResult, subject.GetPathPartIndices("keys"));

            var expectedResult2 = new int[1];
            expectedResult2[0] = 1;
            CollectionAssert.AreEquivalent(expectedResult2, subject.GetPathPartIndices("profile"));
        }

        [TestMethod]
        public void GetPathGetPathPartIndicesWorks_InvalidURI()
        {
            var subject = new ClrUri(":)");
            Assert.AreEqual(null, subject.GetPathPartIndices("keys"));
            Assert.AreEqual(null, subject.GetPathPartIndices(null));
        }

        [TestMethod]
        public void GetPathGetPathPartIndicesWorks_ValidURIInvalidPart()
        {
            var subject = new ClrUri("https://gitlab.com/profile/keys/1579455/keys");
            Assert.AreEqual(null, subject.GetPathPartIndices("test"));
            Assert.AreEqual(null, subject.GetPathPartIndices(null));
        }








    }


}
