﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UriLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClrUriLib_TEST {
	[TestClass]
	public class ClrUriFunctions_Tests {

		#region Scheme
		[TestMethod]
		public void GetSchemeWorks() {
			Assert.AreEqual(null, ClrUriUserFunctions.GetScheme(null));
			Assert.AreEqual(null, ClrUriUserFunctions.GetScheme(string.Empty));
			Assert.AreEqual("http://", ClrUriUserFunctions.GetScheme("http://example.com"));
			Assert.AreEqual("https://", ClrUriUserFunctions.GetScheme("https://example.com"));
			Assert.AreEqual(null, ClrUriUserFunctions.GetScheme("/search/images"));
			Assert.AreEqual(null, ClrUriUserFunctions.GetScheme("?key=value"));
		}
		#endregion Scheme

		#region QueryFunc

		[TestMethod]
        public void GetQueryParametersWorks_NULL()
        {
            var result = ClrUriUserFunctions.GetQueryParameters(null).Cast<KeyValuePair<string, string>>().ToArray();
            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void TSQL_GetQueryParametersWorks_StringEmpty()
        {
            var result = ClrUriUserFunctions.GetQueryParameters(string.Empty).Cast<KeyValuePair<string, string>>().ToArray();
            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void TSQL_GetQueryParametersWorks_GoogleCom()
        {
            var result = ClrUriUserFunctions.GetQueryParameters("https://google.com").Cast<KeyValuePair<string, string>>().ToArray();
            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void TSQL_GetQueryParametersWorks_ValidURI()
        {
            var result = ClrUriUserFunctions
                .GetQueryParameters("https://myuser:mypassword@google.com/search/images?some=arguments&which=are&totaly=nonsense&1=2;or=something;else&or-not")
                .Cast<UriQueryPart>()
                .Select(x => new KeyValuePair<string, string>(x.Key, x.Value))
                .ToArray();

            var expectedParts = new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("some", "arguments"),
                new KeyValuePair<string, string>("which", "are"),
                new KeyValuePair<string, string>("totaly", "nonsense"),
                new KeyValuePair<string, string>("1", "2"),
                new KeyValuePair<string, string>("or", "something"),
                new KeyValuePair<string, string>("else", null),
                new KeyValuePair<string, string>("or-not", null)
            };

            CollectionAssert.AreEquivalent(expectedParts, result);
        }    

        [TestMethod]
        public void TSQL_GetQueryValueWorks_NULLUriNULLParameter()
        {
            var result = ClrUriUserFunctions
                .GetQueryParameterValue(null, null);

            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void TSQL_GetQueryValueWorks_NULLUrinotNULLParameter()
        {
            var result = ClrUriUserFunctions
                .GetQueryParameterValue(null, "param");

            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void TSQL_GetQueryValueWorks_ValidURINULLParameter()
        {
            var result = ClrUriUserFunctions
                .GetQueryParameterValue("https://google.com", null);

            Assert.AreEqual(null, result);

        }

        [TestMethod]
        public void TSQL_GetQueryValueWork_ValidURIExistingParameter()
        {
            var result = ClrUriUserFunctions
                .GetQueryParameterValue("https://www.google.cz/search?q=floor+scatter&as_eq=royal+gainful", "q");

            Assert.AreEqual("floor+scatter", result);
        }

        [TestMethod]
        public void TSQL_GetQueryValueWork_ValidURINonExistingParameter()
        {
            var result = ClrUriUserFunctions
                .GetQueryParameterValue("https://www.google.cz/search?q=floor+scatter&as_eq=royal+gainful", "a");

            Assert.AreEqual(null, result);
        }
    
        [TestMethod]
		public void TSQL_GetQueryValueFast_Works_With_Emptines() {
			XAssert.AreOrdinalEqual((string)null, () => ClrUriUserFunctions.GetQueryParameterValueFast(null, null));
			XAssert.AreOrdinalEqual((string)null, () => ClrUriUserFunctions.GetQueryParameterValueFast(string.Empty, null));
			XAssert.AreOrdinalEqual((string)null, () => ClrUriUserFunctions.GetQueryParameterValueFast(null, string.Empty));
			XAssert.AreOrdinalEqual((string)null, () => ClrUriUserFunctions.GetQueryParameterValueFast(string.Empty, string.Empty));
		}

		[TestMethod]
		public void TSQL_GetQueryValueFast_Works() {
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?a=b", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("&a=b", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast(";a=b", "a"));

			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?a=b&c=d", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("&a=b;c=d", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast(";a=b;c=d", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast(";a=b#c=d", "a"));

			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?aa=1&a=b", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?aa=1;a=b", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?aa=1?a=b", "a"));
			XAssert.IsNull(() => ClrUriUserFunctions.GetQueryParameterValueFast("?aa=1#a=b", "a"));

			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?aa=1&a=b&c=d", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?aa=1;a=b;c=d", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?aa=1?a=b?c=d", "a"));
			XAssert.AreOrdinalEqual("b", () => ClrUriUserFunctions.GetQueryParameterValueFast("?aa=1?a=b#c=d", "a"));
		}


        #endregion QueryFunc

        #region PathFunc

        [TestMethod]
        public void TSQL_GetPathPartsWork_ValidURI()
        {
            var result = ClrUriUserFunctions.GetPathParts("http://www.gardners-eshop.cz/workshopy/")
                .Cast<KeyValuePair<int, string>>()
                .ToArray();

            var expectedParts = new KeyValuePair<int, string>[] {
                new KeyValuePair<int, string>(0, ""),
                new KeyValuePair<int, string>(1, "workshopy"),
                new KeyValuePair<int, string>(2, "")              
            };

            CollectionAssert.AreEquivalent(expectedParts, result);
        }

        [TestMethod]
        public void TSQL_GetPathPartsWork_IncompleteURI()
        {
            var result = ClrUriUserFunctions.GetPathParts("/workshopy/")
                .Cast<KeyValuePair<int, string>>()
                .ToArray();

            var expectedParts = new KeyValuePair<int, string>[] {
                new KeyValuePair<int, string>(0, ""),
                new KeyValuePair<int, string>(1, "workshopy"),
                new KeyValuePair<int, string>(2, "")
            };

            CollectionAssert.AreEquivalent(expectedParts, result);
        }

        [TestMethod]
        public void TSQL_GetPathPartsWork_NULLURI()
        {
            var result = ClrUriUserFunctions.GetPathParts(null)
                .Cast<KeyValuePair<int, string>>()
                .ToArray();

            var expectedParts = new KeyValuePair<int, string>[] {         
            };

            CollectionAssert.AreEquivalent(expectedParts, result);
        }

        [TestMethod]
        public void TSQL_GetPathPartWorks_ValidURIValidIndex()
        {
            var result = ClrUriUserFunctions.GetPathPart("http://www.gardners-eshop.cz/workshopy/", 1);
            Assert.AreEqual("workshopy", result);
        }

        [TestMethod]
        public void TSQL_GetPathPartWorks_IncompleteURIValidIndex()
        {
            var result = ClrUriUserFunctions.GetPathPart("/workshopy/", 1);
            Assert.AreEqual("workshopy", result);

        }

        [TestMethod]
        public void TSQL_GetPathPartWorks_ValidURIInvalidIndex()
        {
            var result = ClrUriUserFunctions.GetPathPart("http://www.gardners-eshop.cz/workshopy/", 10);
            var result2 = ClrUriUserFunctions.GetPathPart("http://www.gardners-eshop.cz/workshopy/", -1);
            Assert.AreEqual(null, result);
            Assert.AreEqual(null, result2);
        }

        //TODO: Subquery test
        //TODO: FirstIndex test
        //TODO: LastIndex test
        //TODO: indices test

        #endregion PathFunc
    }
}
