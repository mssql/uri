﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClrUriLib_TEST {
	[TestClass]
	public class ClrUri_Building_Tests {
		[TestMethod]
		public void WithScheme_Works_On_Null() {
			var empty = ClrUri.Null;
			var edited = empty.WithScheme("http");

			Assert.AreEqual(null, empty.Scheme);
			Assert.AreEqual("http://", edited.Scheme);
		}
	}
}
