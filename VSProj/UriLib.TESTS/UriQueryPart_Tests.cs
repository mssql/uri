﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UriLib.TEST {
	[TestClass]
	public class UriQueryPart_Tests {
		private void AssertEmptiness(UriQueryPart subject) {
			XAssert.IsTrue(() => subject.IsEmpty);
			XAssert.AreEqual((char)0, () => subject.Separator);
			XAssert.IsNullOrEmpty(()=> subject.Key);
			XAssert.IsFalse(() => subject.HasAssignMark);
			XAssert.IsNullOrEmpty(() => subject.Value);
		}

		[TestMethod]
		public void EmptyUriQueryPartIsEmpty() {
			AssertEmptiness(new UriQueryPart());
			AssertEmptiness(new UriQueryPart((char)0, null, false, null));

			AssertEmptiness(new UriQueryPart((char)0, string.Empty, false, null));
			AssertEmptiness(new UriQueryPart((char)0, null, false, string.Empty));
			AssertEmptiness(new UriQueryPart((char)0, string.Empty, false, string.Empty));
		}

		private void AssertNonEmptiness(UriQueryPart subject) {
			XAssert.IsFalse(() => subject.IsEmpty);

			var counter = 0;
			if (subject.Separator != (char)0) { counter++; }
			if (!string.IsNullOrEmpty(subject.Key)) { counter++; }
			if (!string.IsNullOrEmpty(subject.Value)) { counter++; }
			if (subject.HasAssignMark) { counter++; }

			XAssert.IsAtLeast(1, () => counter);
		}

		[TestMethod]
		public void NonEmptyUriQueryPartIsNotEmpty() {
			#region Only assign mark
			AssertNonEmptiness(new UriQueryPart((char)0, null, true, null));

			AssertNonEmptiness(new UriQueryPart((char)0, string.Empty, true, null));
			AssertNonEmptiness(new UriQueryPart((char)0, null, true, string.Empty));
			AssertNonEmptiness(new UriQueryPart((char)0, string.Empty, true, string.Empty));
			#endregion Only assign mark

			#region Only seperator
			foreach (var separator in new char[] { '?', '&', ';' }) {
				AssertNonEmptiness(new UriQueryPart(separator, null, false, null));

				AssertNonEmptiness(new UriQueryPart(separator, string.Empty, false, null));
				AssertNonEmptiness(new UriQueryPart(separator, null, false, string.Empty));
				AssertNonEmptiness(new UriQueryPart(separator, string.Empty, false, string.Empty));
			}
			#endregion Only key

			#region Only seperator
			foreach (var key in new string[] { "term", "page" }) {
				AssertNonEmptiness(new UriQueryPart((char)0, key, false, null));
				AssertNonEmptiness(new UriQueryPart((char)0, key, false, string.Empty));
			}
			#endregion Only key

			#region Only value
			foreach (var value in new string[] { "bla-bla", "12345" }) {
				AssertNonEmptiness(new UriQueryPart((char)0, null, false, value));
				AssertNonEmptiness(new UriQueryPart((char)0, string.Empty, false, value));
			}
			#endregion Only value
		}

		[TestMethod]
		public void WriteWorksOnEmptyQueryPart() {
			var subjects = new UriQueryPart[] {
				new UriQueryPart(),
				new UriQueryPart((char)0, null, false, null),
				new UriQueryPart((char)0, string.Empty, false, null),
				new UriQueryPart((char)0, null, false, string.Empty),
				new UriQueryPart((char)0, string.Empty, false, string.Empty)
			};

			foreach (var subject in subjects) {
				var buffer = new StringBuilder();
				subject.Write(buffer);

				var actual = buffer.ToString();
				XAssert.IsNullOrEmpty(actual);
			}


			var defaultSeparators = new char[] { '?', '&', ';', '+' };
			foreach (var subject in subjects) {
				foreach (var separator in defaultSeparators) {
					var buffer = new StringBuilder();
					subject.Write(buffer, separator);

					var actual = buffer.ToString();
					var expected = separator.ToString();
					XAssert.AreOrdinalEqual(expected, actual);
				}
			}
		}

		[TestMethod]
		public void WriteWorksOnNonEmptyQueryPart() {
			#region Just AssingMark
			{
				var subjects = new UriQueryPart[] {
					new UriQueryPart((char)0, null, true, null),
					new UriQueryPart((char)0, string.Empty, true, null),
					new UriQueryPart((char)0, null, true, string.Empty),
					new UriQueryPart((char)0, string.Empty, true, string.Empty)
				};

				var defaultSeparators = new char[] { '?', '&', ';', '+' };

				foreach (var subject in subjects) {
					foreach (var separator in defaultSeparators) {
						var buffer = new StringBuilder();
						subject.Write(buffer, separator);

						var expected = $"{separator}=";
						var actual = buffer.ToString();
						XAssert.AreOrdinalEqual(expected, actual);
					}
				}
			}
			#endregion Just AssingMark

			#region Just Key
			{
				var subjects = new UriQueryPart[] {
					new UriQueryPart((char)0, "id", false, null),
					new UriQueryPart((char)0, "id", false, string.Empty)
				};

				var defaultSeparators = new char[] { '?', '&', ';', '+' };

				foreach (var subject in subjects) {
					foreach (var separator in defaultSeparators) {
						var buffer = new StringBuilder();
						subject.Write(buffer, separator);

						var expected = $"{separator}{subject.Key}";
						var actual = buffer.ToString();
						XAssert.AreOrdinalEqual(expected, actual);
					}
				}
			}
			#endregion Just Key

			#region Key & AssingMark
			{
				var subjects = new UriQueryPart[] {
					new UriQueryPart((char)0, "id", true, null),
					new UriQueryPart((char)0, "id", true, string.Empty)
				};

				var defaultSeparators = new char[] { '?', '&', ';', '+' };

				foreach (var subject in subjects) {
					foreach (var separator in defaultSeparators) {
						var buffer = new StringBuilder();
						subject.Write(buffer, separator);

						var expected = $"{separator}{subject.Key}=";
						var actual = buffer.ToString();
						XAssert.AreOrdinalEqual(expected, actual);
					}
				}
			}
			#endregion Key & AssingMark

			#region Key & Value (without assign mark)
			{
				var subjects = new UriQueryPart[] {
					new UriQueryPart((char)0, "id", false, "12345"),
					new UriQueryPart((char)0, "id", false, "12345")
				};

				var defaultSeparators = new char[] { '?', '&', ';', '+' };

				foreach (var subject in subjects) {
					foreach (var separator in defaultSeparators) {
						var buffer = new StringBuilder();
						subject.Write(buffer, separator);

						var expected = $"{separator}{subject.Key}={subject.Value}";
						var actual = buffer.ToString();
						XAssert.AreOrdinalEqual(expected, actual);
					}
				}
			}
			#endregion Key & Value (without assign mark)

			#region Key & Value (with assign mark)
			{
				var subjects = new UriQueryPart[] {
					new UriQueryPart((char)0, "id", true, "12345"),
					new UriQueryPart((char)0, "id", true, "12345")
				};

				var defaultSeparators = new char[] { '?', '&', ';', '+' };

				foreach (var subject in subjects) {
					foreach (var separator in defaultSeparators) {
						var buffer = new StringBuilder();
						subject.Write(buffer, separator);

						var expected = $"{separator}{subject.Key}={subject.Value}";
						var actual = buffer.ToString();
						XAssert.AreOrdinalEqual(expected, actual);
					}
				}
			}
			#endregion Key & Value (with assign mark)
		}

		[TestMethod]
		public void SplitToParts_Works() {
			var query = UriQuery.SplitToParts("?newwindow=1&rlz=1C1CHBF_enCZ766CZ766&ei=oEP_WYqSKZLWkwXe87_IBQ&q=uri&oq=uri&gs_l=psy-ab.3..35i39k1j0i67k1l4j0j0i20i263k1j0i67k1l3.221934.222319.0.222532.2.2.0.0.0.0.193.285.1j1.2.0....0...1.1.64.psy-ab..0.2.284....0.N-ZK76_hiAY");

			var part0 = query[0];
			XAssert.AreOrdinalEqual("newwindow", part0.Key);
			XAssert.AreOrdinalEqual("1", part0.Value);

			var part1 = query[1];
			XAssert.AreOrdinalEqual("rlz", part1.Key);
			XAssert.AreOrdinalEqual("1C1CHBF_enCZ766CZ766", part1.Value);

			var part2 = query[2];
			XAssert.AreOrdinalEqual("ei", part2.Key);
			XAssert.AreOrdinalEqual("oEP_WYqSKZLWkwXe87_IBQ", part2.Value);
		}

		[TestMethod]
		public void SplitToParts_Works_2() {
			var query = UriQuery.SplitToParts("?some=arguments&which=are&totaly=nonsense&1=2;or=something;else&or-not");
			var actualParts = query.Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToArray();
			var expectedParts = new KeyValuePair<string, string>[] {
				new KeyValuePair<string, string>("some", "arguments"),
				new KeyValuePair<string, string>("which", "are"),
				new KeyValuePair<string, string>("totaly", "nonsense"),
				new KeyValuePair<string, string>("1", "2"),
				new KeyValuePair<string, string>("or", "something"),
				new KeyValuePair<string, string>("else", null),
				new KeyValuePair<string, string>("or-not", null)
			};

			CollectionAssert.AreEquivalent(expectedParts, actualParts);
		}
	}
}
