﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UriLib.TEST {
	[TestClass]
	public class UriFragment_Tests {
		[TestMethod]
		public void EmptyFragmentIsEmpty() {
			XAssert.IsTrue(() => default(UriFragment).IsEmpty);
			XAssert.IsTrue(() => new UriFragment().IsEmpty);
			XAssert.IsTrue(() => new UriFragment((string)null).IsEmpty);
			XAssert.IsTrue(() => new UriFragment(string.Empty).IsEmpty);
		}

		[TestMethod]
		public void NonEmptyFragmentIsNotEmpty() {
			{
				XAssert.IsFalse(() => new UriFragment("#").IsEmpty);
				XAssert.IsFalse(() => new UriFragment(" ").IsEmpty);
				XAssert.IsFalse(() => new UriFragment("\t").IsEmpty);
				XAssert.IsFalse(() => new UriFragment("\r").IsEmpty);
				XAssert.IsFalse(() => new UriFragment("\n").IsEmpty);
				XAssert.IsFalse(() => new UriFragment("\t \r\n ").IsEmpty);

				XAssert.IsFalse(() => new UriFragment(" ://").IsEmpty);
				XAssert.IsFalse(() => new UriFragment("\t://").IsEmpty);
				XAssert.IsFalse(() => new UriFragment("\r://").IsEmpty);
				XAssert.IsFalse(() => new UriFragment("\n://").IsEmpty);
				XAssert.IsFalse(() => new UriFragment("\t \r\n ://").IsEmpty);
			}
		}

		[TestMethod]
		public void EmptyFragmentReturnsNullUriString() {
			XAssert.AreOrdinalEqual((string)null, () => default(UriFragment).ToUriString());
		}

		[TestMethod]
		public void ValidFragmentIsValid() {
			XAssert.IsTrue(() => new UriFragment().IsValid);
			XAssert.IsTrue(() => new UriFragment(null).IsValid);
			XAssert.IsTrue(() => new UriFragment(string.Empty).IsValid);
			XAssert.IsTrue(() => new UriFragment("#").IsValid);
			XAssert.IsTrue(() => new UriFragment("a").IsValid);
			XAssert.IsTrue(() => new UriFragment("#a").IsValid);
			XAssert.IsTrue(() => new UriFragment("%12345").IsValid);
			XAssert.IsTrue(() => new UriFragment("?/").IsValid);
			XAssert.IsTrue(() => new UriFragment("#?/").IsValid);
			XAssert.IsTrue(() => new UriFragment("#%12345&asdfa12423").IsValid);
			XAssert.IsTrue(() => new UriFragment("asdf?/:@!$&'()*+,;=").IsValid);
			XAssert.IsTrue(() => new UriFragment("#asdf?/:@!$&'()*+,;=").IsValid);
			XAssert.IsTrue(() => new UriFragment("#asdf?/:@!$&'()*+,;=%12345").IsValid);
		}

		[TestMethod]
		public void InvalidFragmentIsNotValid() {
			XAssert.IsFalse(() => new UriHost(" #").IsValid);
			XAssert.IsFalse(() => new UriHost("##").IsValid);
			XAssert.IsFalse(() => new UriHost(" ?").IsValid);
			XAssert.IsFalse(() => new UriHost("#[a").IsValid);
			XAssert.IsFalse(() => new UriHost("asdf:@!$&'()*+,;=[").IsValid);
			XAssert.IsFalse(() => new UriHost("#asdf:@!$&'()*+,;=]").IsValid);
			XAssert.IsFalse(() => new UriHost("#asdf:@!$&'()*+,;=%12345#").IsValid);
		}
	}
}
