﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UriLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UriLib.TEST {
	[TestClass]
	public class Url_Tests {
		private void AssertEmptyUri(Url uri) {
			XAssert.IsTrue(() => uri.IsEmpty);

			XAssert.IsNullOrEmpty(() => uri.Scheme);
			XAssert.IsNullOrEmpty(() => uri.Host);
			XAssert.IsTrue(() => uri.Port.IsEmpty);

			XAssert.IsTrue(() => uri.UserInfo.IsEmpty);

			XAssert.IsTrue(() => uri.Path.IsEmpty);
			XAssert.IsNullOrEmpty(() => uri.Path.Text);

			XAssert.IsTrue(() => uri.Query.IsEmpty);
			XAssert.IsNullOrEmpty(() => uri.Query.Text);

			XAssert.IsNullOrEmpty(() => uri.Fragment);

			XAssert.AreEqual(0, () => uri.PathPartsCount);
			XAssert.AreEqual(0, () => uri.QueryPartsCount);
		}

		private void AssertNonEmptyUri(Url uri) {
			XAssert.IsFalse(() => uri.IsEmpty);

			int counterNontEmpty = 0;
			if (!string.IsNullOrEmpty(uri.Scheme)) { counterNontEmpty++; }
			if (!string.IsNullOrEmpty(uri.Host)) { counterNontEmpty++; }
			if (!string.IsNullOrEmpty(uri.Fragment)) { counterNontEmpty++; }
			if (!uri.Port.IsEmpty) { counterNontEmpty++; }
			if (!uri.UserInfo.IsEmpty) { counterNontEmpty++; }

			if (string.IsNullOrEmpty(uri.Path.Text)) {
				XAssert.IsTrue(() => uri.Path.IsEmpty);
				XAssert.IsNullOrEmpty(() => uri.Path.Parts);
			}
			else {
				XAssert.IsFalse(() => uri.Path.IsEmpty);
				counterNontEmpty++;
			}

			if (string.IsNullOrEmpty(uri.Query.Text)) {
				XAssert.IsTrue(() => uri.Query.IsEmpty);
				XAssert.IsNullOrEmpty(() => uri.Query.Parts);
			}
			else {
				XAssert.IsFalse(() => uri.Query.IsEmpty);
				counterNontEmpty++;
			}

			XAssert.IsAtLeast(1, () => counterNontEmpty);
		}

		private void AssertStrictEqualUri(Url a, Url b) {
			XAssert.AreOrdinalEqual(a.Scheme, b.Scheme);
			XAssert.AreEqual(() => a.UserInfo, () => b.UserInfo);
			XAssert.AreOrdinalEqual(a.Host, b.Host);
			Assert.AreEqual<int?>(a.Port, b.Port);
			XAssert.AreOrdinalEqual(a.Path.Text, b.Path.Text);
			XAssert.AreOrdinalEqual(a.Query.Text, b.Query.Text);
			XAssert.AreOrdinalEqual(a.Fragment, b.Fragment);
		}

		[TestMethod]
		public void EmptyUriIsEmpty() {
			this.AssertEmptyUri(Url.Empty);
			this.AssertEmptyUri(Url.Empty);

			this.AssertEmptyUri(default(Url));
			this.AssertEmptyUri(default(Url));

			this.AssertEmptyUri(new Url());
			this.AssertEmptyUri(new Url());
		}

		[TestMethod]
		public void NonEmptyUriIsNotEmpty() {
			var modifiers = new Func<Url, Url>[] {
				x => x.WithScheme("http"),
				x => x.WithUserName("john"),
				x => x.WithUserPassword("pwd"),
				x => x.WithHost("example.com"),
				x => x.WithPort(12345),
				x => x.WithPath("/search/"),
				x => x.WithQuery("key=value"),
				x => x.WithFragment("my-fragment")
			};

			var empty = Url.Empty;
			foreach (var modifier in modifiers) {
				var subject = modifier(empty);
				AssertNonEmptyUri(subject);
			}
		}

		[TestMethod]
		public void ModificationMethodWorksOnEmptyUri() {
			var empty = Url.Empty;

			{
				var subject0 = empty.WithScheme("http");
				AssertEmptyUri(empty);

				var copy0 = subject0;
				AssertEmptyUri(empty);

				// check for non-emptines
				XAssert.AreEqual("http", () => subject0.Scheme.Name.Text);
				AssertNonEmptyUri(subject0);

				// check for emptines
				var subject1 = subject0.WithoutScheme();
				AssertEmptyUri(subject1);

				// check if subject0 was not modified
				AssertStrictEqualUri(subject0, copy0);
			}

			void AssertModification(Url original, RefFunc<Url> modify, RefFunc<Url> unmodify, RefAct<Url> assert) {
				var originalClone = original; // backup for future

				var modified = modify(ref original);
				var modifiedClone = modified; // backup for future
				AssertStrictEqualUri(originalClone, original); // to be sure, original was not changed

				AssertNonEmptyUri(modified);
				assert(ref modified);

				var unmodified = unmodify(ref modified);
				AssertStrictEqualUri(modified, modifiedClone); // to be sure, original was not changed
				AssertEmptyUri(unmodified);
			}

			#region Scheme
			{
				Url ModifyScheme(ref Url src) => src.WithScheme("http");
				void AssertScheme(ref Url src) => XAssert.AreOrdinalEqual("http", src.Scheme.Name);
				Url UnmodifyScheme(ref Url src) => src.WithoutScheme();

				AssertModification(empty, ModifyScheme, UnmodifyScheme, AssertScheme);
			}
			#endregion Scheme

			#region Host
			{
				Url ModifyHost(ref Url src) => src.WithHost("example.com");
				void AssertHost(ref Url src) => XAssert.AreOrdinalEqual("example.com", src.Host);
				Url UnmodifyHost(ref Url src) => src.WithoutHost();

				AssertModification(empty, ModifyHost, UnmodifyHost, AssertHost);
			}
			#endregion Host

			#region Port
			{
				Url ModifyPort(ref Url src) => src.WithPort(12345);
				void AssertPort(ref Url src) => Assert.AreEqual(12345, src.Port);
				Url UnmodifyPort(ref Url src) => src.WithoutPort();

				AssertModification(empty, ModifyPort, UnmodifyPort, AssertPort);
			}
			#endregion Port

			#region Path
			{
				Url ModifyPath(ref Url src) => src.WithPath("/users/search");
				void AssertPath(ref Url src) => XAssert.AreOrdinalEqual("/users/search", src.Path.Text);
				Url UnmodifyPath(ref Url src) => src.WithoutPath();

				AssertModification(empty, ModifyPath, UnmodifyPath, AssertPath);
			}
			#endregion Path

			#region Query
			{
				Url ModifyQuery(ref Url src) => src.WithQuery("name=value&or=and;bla;foo;bar");
				void AssertQuery(ref Url src) => XAssert.AreOrdinalEqual("name=value&or=and;bla;foo;bar", src.Query.Text);
				Url UnmodifyQuery(ref Url src) => src.WithoutQuery();

				AssertModification(empty, ModifyQuery, UnmodifyQuery, AssertQuery);
			}
			#endregion Query

			#region Fragment
			{
				Url ModifyQuery(ref Url src) => src.WithFragment("some-section-in-article");
				void AssertQuery(ref Url src) => XAssert.AreOrdinalEqual("#some-section-in-article", src.Fragment);
				Url UnmodifyQuery(ref Url src) => src.WithoutFragment();

				AssertModification(empty, ModifyQuery, UnmodifyQuery, AssertQuery);
			}
			#endregion Fragment
		}

		public delegate T RefFunc<T>(ref T input);
		public delegate void RefAct<T>(ref T input);

		[TestMethod]
		public void TryParseWorksWithFullAddressWithoutUserInfo() {
			var source = "https://google.com:443/search/images?newwindow=1&utm_source=asdf;utm_blah#my_fragment";

			var subject = Url.Parse(source).Value;
			XAssert.AreOrdinalEqual("https://", () => subject.Scheme.Text);
			XAssert.AreOrdinalEqual("google.com", () => subject.Host.Text);
			XAssert.AreOrdinalEqual("/search/images", () => subject.Path.Text.Text);
			XAssert.AreOrdinalEqual("?newwindow=1&utm_source=asdf;utm_blah", () => subject.Query.Text.Text);
			XAssert.AreOrdinalEqual("#my_fragment", () => subject.Fragment.Text);
			XAssert.AreEqual(443, () => subject.Port);
			XAssert.IsTrue(() => subject.UserInfo.IsEmpty);
		}

		[TestMethod]
		public void TryParseWorksWithFullAddress() {
			var source = "https://myuser:mypassword@google.com:443/search/images?newwindow=1&utm_source=asdf;utm_blah#my_fragment";

			var subject = Url.Parse(source).Value;
			XAssert.AreOrdinalEqual("https://", () => subject.Scheme.Text);
			XAssert.AreOrdinalEqual("myuser", () => subject.UserInfo.Name);
			XAssert.AreOrdinalEqual("mypassword", () => subject.UserInfo.Password);
			XAssert.AreOrdinalEqual("google.com", () => subject.Host.Text);
			XAssert.AreOrdinalEqual("/search/images", () => subject.Path.Text.Text);
			XAssert.AreOrdinalEqual("?newwindow=1&utm_source=asdf;utm_blah", () => subject.Query.Text.Text);
			XAssert.AreOrdinalEqual("#my_fragment", () => subject.Fragment.Text);
			XAssert.AreEqual(443, () => subject.Port);
		}

		private Url Reserialize(Url uri) {
			using (var mstrm = new System.IO.MemoryStream()) {
				using (var writer = new System.IO.BinaryWriter(mstrm)) {
					uri.Write(writer);
					writer.Flush();

					mstrm.Position = 0;

					using (var reader = new System.IO.BinaryReader(mstrm)) {
						var result = new Url();
						result.Read(reader);
						return result;
					}
				}
			}
		}
		[TestMethod]
		public void Serialization_Works() {
			var orig = Url.Parse("https://google.com:443/search/images?newwindow=1&utm_source=asdf;utm_blah#my_fragment").Value;

			var clone = this.Reserialize(orig);

			XAssert.AreEqual(orig, clone);
		}
	}
}
