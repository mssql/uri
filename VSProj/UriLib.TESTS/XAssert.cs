﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UriLib.TEST {
	public static partial class XAssert {
		[Obsolete("Use 'AreEqual' instead.")]
		public static new bool Equals(object a, object b) {
			throw new InvalidOperationException($"Use '{nameof(AreEqual)}'.");
		}

		#region IsTrue
		public static void IsTrue(bool value) {
			IsTrue(value, $"The '{value}' is true.");
		}
		public static void IsTrue(Expression<Func<bool>> func) {
			var value = func.Compile()();
			IsTrue(value, $"The result of '{func}' is true.");
		}
		public static void IsTrue(bool value, string message) {
			Assert.IsTrue(value, message);
		}
		#endregion IsTrue

		#region IsFalse
		public static void IsFalse(bool value) {
			IsFalse(value, $"The '{value}' is false.");
		}
		public static void IsFalse(Expression<Func<bool>> func) {
			var value = func.Compile()();
			IsFalse(value, $"The result of '{func}' is false.");
		}
		public static void IsFalse(bool value, string message) {
			Assert.IsFalse(value, message);
		}
		#endregion IsFalse

		#region IsNull
		public static void IsNull(object value) {
			IsNull(value, $"The '{value}' is null.");
		}
		public static void IsNull(Expression<Func<object>> func) {
			var value = func.Compile()();
			IsNull(value, $"The result of '{func}' is null.");
		}
		public static void IsNull(object value, string message) {
			Assert.IsNull(value, message);
		}
		#endregion IsNull

		#region IsNullOrEmpty
		public static void IsNullOrEmpty(string value) {
			IsNullOrEmpty(value, $"The '{value}' is null or empty.");
		}
		public static void IsNullOrEmpty(Expression<Func<string>> func) {
			var value = func.Compile()();
			IsNullOrEmpty(value, $"The result of '{func}' is null or empty.");
		}
		public static void IsNullOrEmpty(string value, string message) {
			Assert.IsTrue(string.IsNullOrEmpty(value), message);
		}
		#endregion IsNullOrEmpty

		#region IsNullOrEmpty
		public static void IsNullOrEmpty(Array value) {
			IsNullOrEmpty(value, $"The '{value}' is null or empty array.");
		}
		public static void IsNullOrEmpty(Expression<Func<Array>> func) {
			var value = func.Compile()();
			IsNullOrEmpty(value, $"The result of '{func}' is null or empty array.");
		}
		public static void IsNullOrEmpty(Array value, string message) {
			Assert.IsTrue(value is null || value.Length < 1, message);
		}
		#endregion IsNullOrEmpty

		#region IsNullOrEmpty
		public static void IsNullOrEmpty<T>(IReadOnlyCollection<T> value) {
			IsNullOrEmpty(value, $"The '{value}' is null or empty collection.");
		}
		public static void IsNullOrEmpty<T>(Expression<Func<IReadOnlyCollection<T>>> func) {
			var value = func.Compile()();
			IsNullOrEmpty(value, $"The result of '{func}' is null or empty collection.");
		}
		public static void IsNullOrEmpty<T>(IReadOnlyCollection<T> value, string message) {
			Assert.IsTrue(value is null || value.Count < 1, message);
		}
		#endregion IsNullOrEmpty

		#region IsAtLeast
		public static void IsAtLeast(int minValue, int actualValue) {
			IsAtLeast(minValue, actualValue, $"The '{actualValue}' is at least '{minValue}'.");
		}
		public static void IsAtLeast(int minValue, Expression<Func<int>> actualValueFunc) {
			var value = actualValueFunc.Compile()();
			IsAtLeast(minValue, value, $"The result ({value}) of '{actualValueFunc}' is at least '{minValue}'.");
		}
		public static void IsAtLeast(Expression<Func<int>> minValueFunc, Expression<Func<int>> actualValueFunc) {
			var minValue = actualValueFunc.Compile()();
			var value = actualValueFunc.Compile()();
			IsAtLeast(minValue, value, $"The result ({value}) of '{actualValueFunc}' is at least ({minValue}) '{minValueFunc}'.");
		}
		public static void IsAtLeast(int minValue, int actualValue, string message) {
			Assert.IsTrue(minValue <= actualValue, message);
		}
		#endregion IsAtLeast

		#region AreEqual
		public static void AreEqual<T>(T expected, T actual) {
			var message = $"The expected ({expected}) is equal to actual ({actual}).";
			AreEqual<T>(expected, actual, message);
		}
		public static void AreEqual<T>(T expected, Expression<Func<T>> actualFunc) {
			var actualValue = actualFunc.Compile()();
			var message = $"The expected ({expected}) is equal to result ({actualValue}) of '{actualFunc}'.";
			AreEqual<T>(expected, actualValue, message);
		}
		public static void AreEqual<T>(Expression<Func<T>> expectedFunc, T actual) {
			var expectedValue = expectedFunc.Compile()();
			var message = $"The expected result ({expectedFunc}) of '{expectedFunc}' is equal to actual ({actual}).";
			AreEqual<T>(expectedValue, actual, message);
		}
		public static void AreEqual<T>(Expression<Func<T>> expectedFunc, Expression<Func<T>> actualFunc) {
			var expectedValue = expectedFunc.Compile()();
			var actualValue = actualFunc.Compile()();
			var message = $"The expected result ({expectedValue}) of '{expectedFunc}' is equal to result ({actualValue}) of '{actualFunc}'.";
			AreEqual<T>(expectedValue, actualValue, message);
		}
		public static void AreEqual<T>(T expected, T actual, string message) {
			Assert.AreEqual<T>(expected, actual, message);
		}
		#endregion AreEqual

		#region AreOrdinalEqual
		public static void AreOrdinalEqual(string expected, string actual) {
			var message = $"The expected string ({expected}) is ordinaly equal to actual string ({actual}).";
			AreOrdinalEqual(expected, actual, message);
		}
		public static void AreOrdinalEqual(string expected, Expression<Func<string>> actualFunc) {
			var actualValue = actualFunc.Compile()();
			var message = $"The expected string ({expected}) is ordinaly equal to result of '{actualFunc}' ({actualValue}).";
			AreOrdinalEqual(expected, actualValue, message);
		}
		public static void AreOrdinalEqual(Expression<Func<string>> expectedFunc, string actual) {
			var expectedValue = expectedFunc.Compile()();
			var message = $"The expected result of '{expectedFunc}' ({expectedValue}) is ordinaly equal to actual string ({actual}).";
			AreOrdinalEqual(expectedValue, actual, message);
		}
		public static void AreOrdinalEqual(Expression<Func<string>> expectedFunc, Expression<Func<string>> actualFunc) {
			var expectedValue = expectedFunc.Compile()();
			var actualValue = actualFunc.Compile()();
			var message = $"The expected result of '{expectedFunc}' ({expectedValue}) is ordinaly equal to result of '{actualFunc}' ({actualValue}).";
			AreOrdinalEqual(expectedValue, actualValue, message);
		}
		public static void AreOrdinalEqual(string expected, string actual, string message) {
			Assert.AreEqual<int>(0, StringComparer.Ordinal.Compare(expected, actual), message);
		}
		#endregion AreOrdinalEqual

		#region ItemsAreEqual
		public static void ItemsAreEqualInOrder(System.Collections.IEnumerable expected, System.Collections.IEnumerable actual) {
			ItemsAreEqualInOrder(expected, actual, (string)null);
		}
		public static void ItemsAreEqualInOrder(System.Collections.IEnumerable expected, System.Collections.IEnumerable actual, string message) {
			var expectedArray = expected?.OfType<object>()?.ToArray();
			var actualArray = actual?.OfType<object>()?.ToArray();

			var expectedLength = expectedArray?.Length ?? 0;
			var actualLength = actualArray?.Length ?? 0;

			if (expectedLength != actualLength) {
				Assert.Fail($"Expected length ({expectedLength}) != actual length ({actualLength}). {message}");
			}
			if (expectedLength == 0) {
				// no items => are equal
				return;
			}

			for (var i = 0; i < expectedArray.Length; i++) {
				var expectedItem = expectedArray[i];
				var actualItem = actualArray[i];

				if (object.Equals(actualItem, expectedItem) || object.Equals(expectedItem, actualItem)) {
					// two items are equal => go for next items
					continue;
				}

				Assert.Fail($"On index ({i}) is expected item ({expectedItem}) different than actual item ({actualItem}). {message}");
			}
		}
		#endregion ItemsAreEqual
	}
}
