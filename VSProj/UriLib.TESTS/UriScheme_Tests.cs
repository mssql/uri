﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UriLib.TEST {
	[TestClass]
	public class UriScheme_Tests {
		[TestMethod]
		public void EmptySchemeIsEmpty() {
			XAssert.IsTrue(() => default(UriScheme).IsEmpty);
			XAssert.IsTrue(() => new UriScheme().IsEmpty);
			XAssert.IsTrue(() => new UriScheme((string)null).IsEmpty);
			XAssert.IsTrue(() => new UriScheme(string.Empty).IsEmpty);
		}

		[TestMethod]
		public void GetName_Works() {
			XAssert.AreOrdinalEqual("a", () => UriScheme.GetName("a"));
			XAssert.AreOrdinalEqual("a", () => UriScheme.GetName("a://"));
			XAssert.AreOrdinalEqual("  a", () => UriScheme.GetName("  a://"));
			XAssert.AreOrdinalEqual("  a ", () => UriScheme.GetName("  a ://"));
		}

		[TestMethod]
		public void NonEmptySchemeIsNotEmpty() {
			{
				XAssert.IsFalse(() => new UriScheme(" ").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("\t").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("\r").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("\n").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("\t \r\n ").IsEmpty);

				XAssert.IsFalse(() => new UriScheme(" ://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("\t://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("\r://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("\n://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("\t \r\n ://").IsEmpty);
			}

			{
				XAssert.IsFalse(() => new UriScheme("a").IsEmpty);
				XAssert.IsFalse(() => new UriScheme(" a").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("a ").IsEmpty);
				XAssert.IsFalse(() => new UriScheme(" a ").IsEmpty);

				XAssert.IsFalse(() => new UriScheme("ftp").IsEmpty);
				XAssert.IsFalse(() => new UriScheme(" ftp").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("ftp ").IsEmpty);
				XAssert.IsFalse(() => new UriScheme(" ftp ").IsEmpty);
			}

			{
				XAssert.IsFalse(() => new UriScheme("a://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme(" a://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("a ://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme(" a ://").IsEmpty);

				XAssert.IsFalse(() => new UriScheme("ftp://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme(" ftp://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme("ftp ://").IsEmpty);
				XAssert.IsFalse(() => new UriScheme(" ftp ://").IsEmpty);
			}
		}

		[TestMethod]
		public void EmptySchemeReturnsNullUriString() {
			XAssert.AreOrdinalEqual((string)null, () => default(UriScheme).ToUriString());
		}

		[TestMethod]
		public void NameIsExtractedProperly() {
			var scheme = new UriScheme(" http ://");
			var name = scheme.Name;
			XAssert.AreOrdinalEqual(" http ", name);
		}

		[TestMethod]
		public void ValidSchemeIsValid() {
			XAssert.IsTrue(() => new UriScheme("http").IsValid);
			XAssert.IsTrue(() => new UriScheme("http://").IsValid);
			XAssert.IsTrue(() => new UriScheme("ftp").IsValid);
			XAssert.IsTrue(() => new UriScheme("ftp://").IsValid);
			XAssert.IsTrue(() => new UriScheme("").IsValid);
			XAssert.IsTrue(() => new UriScheme(null).IsValid);
			XAssert.IsTrue(() => new UriScheme().IsValid);
		}

		[TestMethod]
		public void _Http_Scheme_IsInvalidAndCanBeRepaired() {
			var orig = new UriScheme(" http ");

			Assert.IsFalse(orig.IsValid);

			var repaired = orig.Validate().Repair(orig);

			Assert.IsTrue(repaired.IsValid);
			Assert.AreEqual("http", repaired.Name.ToString());
		}

		[TestMethod]
		public void WeirdSchemeIsRepairableAndValid() {
			var orig = new UriScheme(" h.t+t-p1s0 ://");

			Assert.IsFalse(orig.IsValid);

			var repaired = orig.Validate().Repair(orig);

			Assert.IsTrue(repaired.IsValid);
			Assert.AreEqual("h.t+t-p1s0", repaired.Name.ToString());
		}
	}
}
