﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UriLib.TEST {
	[TestClass]
	public class UriPath_Tests {
		[TestMethod]
		public void EmptyUriPathIsEmpty() {
			var empty = new UriPath();

			XAssert.IsTrue(() => empty.IsEmpty);
			XAssert.IsNull(() => empty.Text.Original);
			XAssert.IsNull(() => empty.Parts);
			XAssert.AreEqual(0, () => empty.PartsCount);
		}

		private void AssertParsedPath(string path, string[] expectedParts) {
			var subject = new UriPath(path);

			XAssert.IsFalse(() => subject.IsEmpty);
			XAssert.AreOrdinalEqual(path, subject.Text);
			XAssert.ItemsAreEqualInOrder(expectedParts, subject.Parts);

			var producedUriPath = subject.ToUriString();
			XAssert.AreOrdinalEqual(path, producedUriPath);
		}

		[TestMethod]
		public void SingleWordIsParsedCorrectly() {
			var subject = new UriPath("search");

			XAssert.IsFalse(() => subject.IsEmpty);
			XAssert.AreOrdinalEqual("search", () => subject.Text);
			XAssert.ItemsAreEqualInOrder(new string[] { "search" }, subject.Parts);
		}

		[TestMethod]
		public void ParsingWorks() {
			AssertParsedPath("search", new string[] { "search" });
			AssertParsedPath("/search", new string[] { "", "search" });
			AssertParsedPath("search/", new string[] { "search", "" });
			AssertParsedPath("/search/", new string[] { "", "search", "" });

			AssertParsedPath("users/search", new string[] { "users", "search" });
			AssertParsedPath("/users/search", new string[] { "", "users", "search" });
			AssertParsedPath("users/search/", new string[] { "users", "search", "" });
			AssertParsedPath("/users/search/", new string[] { "", "users", "search", "" });

			AssertParsedPath("users/search/articles", new string[] { "users", "search", "articles" });
			AssertParsedPath("/users/search/articles", new string[] { "", "users", "search", "articles" });
			AssertParsedPath("users/search/articles/", new string[] { "users", "search", "articles", "" });
			AssertParsedPath("/users/search/articles/", new string[] { "", "users", "search", "articles", "" });
		}

		[TestMethod]
		public void NullPartsWorks() {
			var subject = new UriPath((string[])null);
			XAssert.IsTrue(() => subject.IsEmpty);
			XAssert.IsNull(() => subject.Text.Original);
			XAssert.IsNull(() => subject.Parts);
			XAssert.IsNull(() => subject.ToUriString());
		}

		[TestMethod]
		public void EmptyPartsWorks() {
			var subject = new UriPath(new string[0]);
			XAssert.IsTrue(() => subject.IsEmpty);
			XAssert.IsNull(() => subject.Text.Original);
			XAssert.ItemsAreEqualInOrder(new string[0], subject.Parts);
			XAssert.IsNull(() => subject.ToUriString());
		}

		private void AssertConstructedPath(string[] parts, string expectedPath) {
			var clonedParts = (string[])parts.Clone();
			var subject = new UriPath(clonedParts);
			CollectionAssert.AreEquivalent(clonedParts, parts); // check if original 'parts' is not edited

			XAssert.IsFalse(() => subject.IsEmpty);
			XAssert.AreOrdinalEqual(expectedPath, () => subject.Text);
			XAssert.ItemsAreEqualInOrder(clonedParts, subject.Parts);

			var producedUriPath = subject.ToUriString();
			XAssert.AreOrdinalEqual(expectedPath, producedUriPath);
		}

		[TestMethod]
		public void ConstructingWorks() {
			AssertConstructedPath(new string[] { "search" }, "search");
			AssertConstructedPath(new string[] { "", "search" }, "/search");
			AssertConstructedPath(new string[] { "search", "" }, "search/");
			AssertConstructedPath(new string[] { "", "search", "" }, "/search/");

			AssertConstructedPath(new string[] { "users", "search" }, "users/search");
			AssertConstructedPath(new string[] { "", "users", "search" }, "/users/search");
			AssertConstructedPath(new string[] { "users", "search", "" }, "users/search/");
			AssertConstructedPath(new string[] { "", "users", "search", "" }, "/users/search/");

			AssertConstructedPath(new string[] { "users", "search", "articles" }, "users/search/articles");
			AssertConstructedPath(new string[] { "", "users", "search", "articles" }, "/users/search/articles");
			AssertConstructedPath(new string[] { "users", "search", "articles", "" }, "users/search/articles/");
			AssertConstructedPath(new string[] { "", "users", "search", "articles", "" }, "/users/search/articles/");

			AssertConstructedPath(new string[] { "", "users", "", "search", "", "articles", "" }, "/users//search//articles/");
		}

		[TestMethod]
		public void ValidPathIsValid() {
			XAssert.IsTrue(() => new UriPath().IsValid);
			XAssert.IsTrue(() => new UriPath((string)null).IsValid);
			XAssert.IsTrue(() => new UriPath(string.Empty).IsValid);

			XAssert.IsTrue(() => new UriPath("search").IsValid);
			XAssert.IsTrue(() => new UriPath("search/").IsValid);
			XAssert.IsTrue(() => new UriPath("/search").IsValid);
			XAssert.IsTrue(() => new UriPath("/search/").IsValid);

			XAssert.IsTrue(() => new UriPath("search/images").IsValid);
			XAssert.IsTrue(() => new UriPath("search/images/").IsValid);
			XAssert.IsTrue(() => new UriPath("/search/images").IsValid);
			XAssert.IsTrue(() => new UriPath("/search/images/").IsValid);

			XAssert.IsTrue(() => new UriPath("search/images.jpg").IsValid);
			XAssert.IsTrue(() => new UriPath("search/images/.jpg").IsValid);
			XAssert.IsTrue(() => new UriPath("/search/images.jpg").IsValid);
			XAssert.IsTrue(() => new UriPath("/search/images/.jpg").IsValid);

			XAssert.IsTrue(() => new UriPath("search/i-mages.jpg").IsValid);
			XAssert.IsTrue(() => new UriPath("search/im-ages/.jpg").IsValid);
			XAssert.IsTrue(() => new UriPath("/search/im-ages.jpg").IsValid);
			XAssert.IsTrue(() => new UriPath("/search/ima-ges/.jpg").IsValid);
		}

		[TestMethod]
		public void InvalidPathIsInvalid() {
			XAssert.IsFalse(() => new UriPath(" ").IsValid);
		}
	}
}
