﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UriLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UriLib.Support;

namespace UriLib.TEST {
	[TestClass]
	public class StrSpan_Tests {
		[TestMethod]
		public void TrimStart_Works() {
			XAssert.AreOrdinalEqual((string)null, () => default(StrSpan).TrimStart());
			XAssert.AreOrdinalEqual((string)null, () => new StrSpan(string.Empty).TrimStart());

			XAssert.AreOrdinalEqual((string)null, () => new StrSpan(" ").TrimStart());
			XAssert.AreOrdinalEqual((string)null, () => new StrSpan("  ").TrimStart());
			XAssert.AreOrdinalEqual((string)null, () => new StrSpan("\t\r\n").TrimStart());

			XAssert.AreOrdinalEqual("a", () => new StrSpan(" a").TrimStart());
			XAssert.AreOrdinalEqual("a", () => new StrSpan("  a").TrimStart());
			XAssert.AreOrdinalEqual("a", () => new StrSpan("\t\r\na").TrimStart());

			XAssert.AreOrdinalEqual("asdf", () => new StrSpan(" asdf").TrimStart());
			XAssert.AreOrdinalEqual("asdf", () => new StrSpan("  asdf").TrimStart());
			XAssert.AreOrdinalEqual("asdf", () => new StrSpan("\t\r\nasdf").TrimStart());

			XAssert.AreOrdinalEqual("a ", () => new StrSpan(" a ").TrimStart());
			XAssert.AreOrdinalEqual("a  ", () => new StrSpan("  a  ").TrimStart());
			XAssert.AreOrdinalEqual("a\t\r\n", () => new StrSpan("\t\r\na\t\r\n").TrimStart());

			XAssert.AreOrdinalEqual("asdf ", () => new StrSpan(" asdf ").TrimStart());
			XAssert.AreOrdinalEqual("asdf  ", () => new StrSpan("  asdf  ").TrimStart());
			XAssert.AreOrdinalEqual("asdf\t\r\n", () => new StrSpan("\t\r\nasdf\t\r\n").TrimStart());

			XAssert.AreOrdinalEqual("a", () => new StrSpan("a").TrimStart());
			XAssert.AreOrdinalEqual("as", () => new StrSpan("as").TrimStart());
			XAssert.AreOrdinalEqual("asdf", () => new StrSpan("asdf").TrimStart());
		}

		[TestMethod]
		public void TrimEnd_Works() {
			XAssert.AreOrdinalEqual((string)null, () => default(StrSpan).TrimEnd());
			XAssert.AreOrdinalEqual((string)null, () => new StrSpan(string.Empty).TrimEnd());

			XAssert.AreOrdinalEqual((string)null, () => new StrSpan(" ").TrimEnd());
			XAssert.AreOrdinalEqual((string)null, () => new StrSpan("  ").TrimEnd());
			XAssert.AreOrdinalEqual((string)null, () => new StrSpan("\t\r\n").TrimEnd());

			XAssert.AreOrdinalEqual("a", () => new StrSpan("a ").TrimEnd());
			XAssert.AreOrdinalEqual("a", () => new StrSpan("a  ").TrimEnd());
			XAssert.AreOrdinalEqual("a", () => new StrSpan("a\t\r\n").TrimEnd());

			XAssert.AreOrdinalEqual(" a", () => new StrSpan(" a").TrimEnd());
			XAssert.AreOrdinalEqual("  a", () => new StrSpan("  a").TrimEnd());
			XAssert.AreOrdinalEqual("\t\r\na", () => new StrSpan("\t\r\na").TrimEnd());

			XAssert.AreOrdinalEqual(" asdf", () => new StrSpan(" asdf").TrimEnd());
			XAssert.AreOrdinalEqual("  asdf", () => new StrSpan("  asdf").TrimEnd());
			XAssert.AreOrdinalEqual("\t\r\nasdf", () => new StrSpan("\t\r\nasdf").TrimEnd());

			XAssert.AreOrdinalEqual(" a", () => new StrSpan(" a ").TrimEnd());
			XAssert.AreOrdinalEqual("  a", () => new StrSpan("  a  ").TrimEnd());
			XAssert.AreOrdinalEqual("\t\r\na", () => new StrSpan("\t\r\na\t\r\n").TrimEnd());

			XAssert.AreOrdinalEqual(" asdf", () => new StrSpan(" asdf ").TrimEnd());
			XAssert.AreOrdinalEqual("  asdf", () => new StrSpan("  asdf  ").TrimEnd());
			XAssert.AreOrdinalEqual("\t\r\nasdf", () => new StrSpan("\t\r\nasdf\t\r\n").TrimEnd());

			XAssert.AreOrdinalEqual("a", () => new StrSpan("a").TrimEnd());
			XAssert.AreOrdinalEqual("as", () => new StrSpan("as").TrimEnd());
			XAssert.AreOrdinalEqual("asdf", () => new StrSpan("asdf").TrimEnd());
		}

		[TestMethod]
		public void UntilWorks_On_SimpleString() {
			StrSpan text = "okolo frýdku cestička";

			XAssert.AreOrdinalEqual("okolo ", () => text.Until("f"));
			XAssert.AreOrdinalEqual("okolo ", () => text.Until("frýd"));
			XAssert.AreOrdinalEqual("okolo ", () => text.Until("frýdku cestička"));

			XAssert.AreOrdinalEqual("okolo frý", () => text.Until("d"));
			XAssert.AreOrdinalEqual("okolo frý", () => text.Until("dku"));
			XAssert.AreOrdinalEqual("okolo frý", () => text.Until("dku cestička"));

			XAssert.AreOrdinalEqual("okolo frýdku cestička", () => text.Until("xxx"));

			XAssert.AreOrdinalEqual(" http ", () => new StrSpan(" http ://").Until("://"));
		}

		[TestMethod]
		public void UntilWorks_On_SpannedString() {
			var texts = new StrSpan[] {
				new StrSpan("okolo frýdku cestička, a na ní se zelená travička", 0, 21),
				new StrSpan("bla" + "okolo frýdku cestička, a na ní se zelená travička", 3, 21)
			};

			foreach (var text in texts) {
				XAssert.AreOrdinalEqual("okolo ", () => text.Until("f"));
				XAssert.AreOrdinalEqual("okolo ", () => text.Until("frýd"));
				XAssert.AreOrdinalEqual("okolo ", () => text.Until("frýdku cestička"));

				XAssert.AreOrdinalEqual("okolo frý", () => text.Until("d"));
				XAssert.AreOrdinalEqual("okolo frý", () => text.Until("dku"));
				XAssert.AreOrdinalEqual("okolo frý", () => text.Until("dku cestička"));

				XAssert.AreOrdinalEqual(text.Text, () => text.Until("xxx"));
			}
		}

		[TestMethod]
		public void UntilFunc_Works_On_SimpleString() {
			StrSpan text = "?q=a&q=b;q=c?q=d#f";

			XAssert.AreOrdinalEqual(string.Empty, text.Until(ch => ch != '?'));
			XAssert.AreOrdinalEqual("?", text.Until(ch => ch != 'q'));
			XAssert.AreOrdinalEqual("?q", text.Until(ch => ch != '='));
			XAssert.AreOrdinalEqual("?q=", text.Until(ch => ch != 'a'));
			XAssert.AreOrdinalEqual("?q=a", text.Until(ch => ch != '&'));
			XAssert.AreOrdinalEqual("?q=a&q=", text.Until(ch => ch != 'b'));
			XAssert.AreOrdinalEqual("?q=a&q=b", text.Until(ch => ch != ';'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=", text.Until(ch => ch != 'c'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=", text.Until(ch => ch != 'd'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=d", text.Until(ch => ch != '#'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=d#", text.Until(ch => ch != 'f'));
		}

		[TestMethod]
		public void UntilFunc_Works_On_SpannedString() {
			var prefix = "bla ? bla ?";
			StrSpan text = new StrSpan(prefix + "?q=a&q=b;q=c?q=d#f", prefix.Length);

			XAssert.AreOrdinalEqual(string.Empty, text.Until(ch => ch != '?'));
			XAssert.AreOrdinalEqual("?", text.Until(ch => ch != 'q'));
			XAssert.AreOrdinalEqual("?q", text.Until(ch => ch != '='));
			XAssert.AreOrdinalEqual("?q=", text.Until(ch => ch != 'a'));
			XAssert.AreOrdinalEqual("?q=a", text.Until(ch => ch != '&'));
			XAssert.AreOrdinalEqual("?q=a&q=", text.Until(ch => ch != 'b'));
			XAssert.AreOrdinalEqual("?q=a&q=b", text.Until(ch => ch != ';'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=", text.Until(ch => ch != 'c'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=", text.Until(ch => ch != 'd'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=d", text.Until(ch => ch != '#'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=d#", text.Until(ch => ch != 'f'));
		}

		[TestMethod]
		public void UntilFunc_Works_On_SpannedString_2() {
			var prefix = "bla ? bla ?";
			var suffix = "foo bar";
			var innerText = "?q=a&q=b;q=c?q=d#f";
			StrSpan text = new StrSpan(prefix + innerText + suffix, prefix.Length, innerText.Length);

			XAssert.AreOrdinalEqual(string.Empty, text.Until(ch => ch != '?'));
			XAssert.AreOrdinalEqual("?", text.Until(ch => ch != 'q'));
			XAssert.AreOrdinalEqual("?q", text.Until(ch => ch != '='));
			XAssert.AreOrdinalEqual("?q=", text.Until(ch => ch != 'a'));
			XAssert.AreOrdinalEqual("?q=a", text.Until(ch => ch != '&'));
			XAssert.AreOrdinalEqual("?q=a&q=", text.Until(ch => ch != 'b'));
			XAssert.AreOrdinalEqual("?q=a&q=b", text.Until(ch => ch != ';'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=", text.Until(ch => ch != 'c'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=", text.Until(ch => ch != 'd'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=d", text.Until(ch => ch != '#'));
			XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=d#", text.Until(ch => ch != 'f'));
		}

		[TestMethod]
		public void SkipUntil_Works_On() {
			StrSpan[] texts;
			{
				var prefix = "bla ? bla ?";
				var suffix = "foo bar";
				var innerText = "?q=a&q=b;q=c?q=d#f";

				texts = new StrSpan[] {
					innerText,
					new StrSpan(prefix + innerText, prefix.Length),
					new StrSpan(prefix + innerText + suffix, prefix.Length, innerText.Length)
				};
			}

			foreach (var text in texts) {
				XAssert.AreOrdinalEqual(string.Empty, text.SkipUntil(ch => ch != '%'));
				XAssert.AreOrdinalEqual("?q=a&q=b;q=c?q=d#f", text.SkipUntil(ch => ch != '?'));
				XAssert.AreOrdinalEqual("q=a&q=b;q=c?q=d#f", text.SkipUntil(ch => ch != 'q'));
				XAssert.AreOrdinalEqual("=a&q=b;q=c?q=d#f", text.SkipUntil(ch => ch != '='));
				XAssert.AreOrdinalEqual("a&q=b;q=c?q=d#f", text.SkipUntil(ch => ch != 'a'));
				XAssert.AreOrdinalEqual("&q=b;q=c?q=d#f", text.SkipUntil(ch => ch != '&'));
				XAssert.AreOrdinalEqual("b;q=c?q=d#f", text.SkipUntil(ch => ch != 'b'));
				XAssert.AreOrdinalEqual(";q=c?q=d#f", text.SkipUntil(ch => ch != ';'));
				XAssert.AreOrdinalEqual("c?q=d#f", text.SkipUntil(ch => ch != 'c'));
				XAssert.AreOrdinalEqual("d#f", text.SkipUntil(ch => ch != 'd'));
				XAssert.AreOrdinalEqual("#f", text.SkipUntil(ch => ch != '#'));
				XAssert.AreOrdinalEqual("f", text.SkipUntil(ch => ch != 'f'));
			}
		}
	}
}
