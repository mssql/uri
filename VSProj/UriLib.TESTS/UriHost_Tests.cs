﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UriLib.TEST {
	[TestClass]
	public class UriHost_Tests {
		[TestMethod]
		public void EmptyHostIsEmpty() {
			XAssert.IsTrue(() => default(UriHost).IsEmpty);
			XAssert.IsTrue(() => new UriHost().IsEmpty);
			XAssert.IsTrue(() => new UriHost((string)null).IsEmpty);
			XAssert.IsTrue(() => new UriHost(string.Empty).IsEmpty);
		}

		[TestMethod]
		public void NonEmptyHostIsNotEmpty() {
			{
				XAssert.IsFalse(() => new UriHost(" ").IsEmpty);
				XAssert.IsFalse(() => new UriHost("\t").IsEmpty);
				XAssert.IsFalse(() => new UriHost("\r").IsEmpty);
				XAssert.IsFalse(() => new UriHost("\n").IsEmpty);
				XAssert.IsFalse(() => new UriHost("\t \r\n ").IsEmpty);

				XAssert.IsFalse(() => new UriHost(" ://").IsEmpty);
				XAssert.IsFalse(() => new UriHost("\t://").IsEmpty);
				XAssert.IsFalse(() => new UriHost("\r://").IsEmpty);
				XAssert.IsFalse(() => new UriHost("\n://").IsEmpty);
				XAssert.IsFalse(() => new UriHost("\t \r\n ://").IsEmpty);
			}

			{
				XAssert.IsFalse(() => new UriHost("a").IsEmpty);
				XAssert.IsFalse(() => new UriHost(" a").IsEmpty);
				XAssert.IsFalse(() => new UriHost("a ").IsEmpty);
				XAssert.IsFalse(() => new UriHost(" a ").IsEmpty);

				XAssert.IsFalse(() => new UriHost("ftp").IsEmpty);
				XAssert.IsFalse(() => new UriHost(" ftp").IsEmpty);
				XAssert.IsFalse(() => new UriHost("ftp ").IsEmpty);
				XAssert.IsFalse(() => new UriHost(" ftp ").IsEmpty);
			}

			{
				XAssert.IsFalse(() => new UriHost("a://").IsEmpty);
				XAssert.IsFalse(() => new UriHost(" a://").IsEmpty);
				XAssert.IsFalse(() => new UriHost("a ://").IsEmpty);
				XAssert.IsFalse(() => new UriHost(" a ://").IsEmpty);

				XAssert.IsFalse(() => new UriHost("ftp://").IsEmpty);
				XAssert.IsFalse(() => new UriHost(" ftp://").IsEmpty);
				XAssert.IsFalse(() => new UriHost("ftp ://").IsEmpty);
				XAssert.IsFalse(() => new UriHost(" ftp ://").IsEmpty);
			}
		}

		[TestMethod]
		public void EmptyHostReturnsNullUriString() {
			XAssert.AreOrdinalEqual((string)null, () => default(UriHost).ToUriString());
		}

		[TestMethod]
		public void ValidHostIsValid() {
			XAssert.IsTrue(() => new UriHost(".").IsValid);
			XAssert.IsTrue(() => new UriHost("12345").IsValid);
			XAssert.IsTrue(() => new UriHost("example.com").IsValid);
			XAssert.IsTrue(() => new UriHost("127.0.0.1").IsValid);
			XAssert.IsTrue(() => new UriHost("a.b-c.d.e.f").IsValid);
			XAssert.IsTrue(() => new UriHost("a.b-c.d.e.f.").IsValid);
			XAssert.IsTrue(() => new UriHost(null).IsValid);
			XAssert.IsTrue(() => new UriHost().IsValid);
		}

		[TestMethod]
		public void InvalidHostIsNotValid() {
			XAssert.IsFalse(() => new UriHost(" .").IsValid);
			XAssert.IsFalse(() => new UriHost("1?2345").IsValid);
			XAssert.IsFalse(() => new UriHost("ex:ample.com").IsValid);
			XAssert.IsFalse(() => new UriHost("127#.0.0.1").IsValid);
			XAssert.IsFalse(() => new UriHost("a.b.[c.d.e.f").IsValid);
			XAssert.IsFalse(() => new UriHost("a.b.c@.d.e.f.").IsValid);
		}
	}
}
