using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

using UriLib;

public partial class ClrUriUserFunctions {
	/// <summary>
	/// Validate incoming string as URI.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	private static ClrUri ValidateAndReturnClrUri(object sqlUriValue) {
		ClrUri uri;
		if (sqlUriValue is null || sqlUriValue is DBNull) {
			uri = ClrUri.Null;
		}
		else if (sqlUriValue is string str) {
			uri = new ClrUri(str);
		}
		else if (sqlUriValue is SqlString sqlStr) {
			uri = new ClrUri(sqlStr.Value);
		}
		else {
			var errMsg = $"Cannot convert '{sqlUriValue.GetType().FullName}' to '{typeof(ClrUri).FullName}' or '{typeof(string).FullName}'.";
			throw new ArgumentException(errMsg, nameof(sqlUriValue));
		}

		return uri;
	}

	#region SchemeFunc 
	/// <summary>
	/// Returns URI Scheme property value if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetScheme"
		, IsPrecise = true
		, IsDeterministic = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetScheme(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).ParsedValue.Scheme.ToString();
	}

	/// <summary>
	/// Returns URI Scheme property name if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetSchemeName"
		, IsPrecise = true
		, IsDeterministic = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetSchemeName(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).ParsedValue.Scheme.Name.ToString();
	}

	#endregion SchemeFunc

	#region UserInfoFunc

	/// <summary>
	/// Returns URI UserName property value if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetUserName"
		, IsPrecise = true
		, IsDeterministic = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetUserName(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).ParsedValue.UserInfo.Name.ToString();
	}

	/// <summary>
	/// Returns URI UserPassword property value if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetUserPassword"
		, IsPrecise = true
		, IsDeterministic = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetUserPassword(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).ParsedValue.UserInfo.Password.ToString();
	}

	/// <summary>
	/// Returns URI UserInfo property value if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetUserInfo"
		, IsPrecise = true
		, IsDeterministic = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetUserInfo(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).ParsedValue.UserInfo.ToUriString();
	}

	#endregion UserInfoFunc

	#region HostPortFunc

	/// <summary>
	/// Returns URI Host property value if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetHost"
	   , IsPrecise = true
	   , IsDeterministic = true
	   , DataAccess = DataAccessKind.None
	   , SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetHost(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).ParsedValue.Host.ToString();
	}

	/// <summary>
	/// Returns URI Port property value if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetPort"
	  , IsPrecise = true
	  , IsDeterministic = true
	  , DataAccess = DataAccessKind.None
	  , SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetPort(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).ParsedValue.Port.ToUriString();
	}

	/// <summary>
	/// Returns URI Host property value with URI Port property value as string if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetHostWithPort"
	  , IsPrecise = true
	  , IsDeterministic = true
	  , DataAccess = DataAccessKind.None
	  , SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetHostWithPort(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).HostWithPort;
	}

	#endregion HostPortFunc

	#region PathFunc

	/// <summary>
	/// Helper function defining result table for GetPathParts Table-valued function.
	/// </summary>
	/// <param name="row"></param>
	/// <param name="index"></param>
	/// <param name="pathPart"></param>
	public static void GetPathParts_RowFunction(object row, out int index, out string pathPart) {
		var part = (KeyValuePair<int, string>)row;
		index = part.Key;
		pathPart = part.Value;
	}

	/// <summary>
	/// Returns table with list of all URI Path property parts, together with their position (index).
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetPathParts"
		, TableDefinition = "[Index] int, [PathPart] nvarchar(max)"
		, FillRowMethodName = nameof(GetPathParts_RowFunction)
		, IsPrecise = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static System.Collections.IEnumerable GetPathParts(object sqlUriValue) {
		var uri = ValidateAndReturnClrUri(sqlUriValue);
		return (System.Collections.IEnumerable)uri.GetPartParts() ?? Array.Empty<KeyValuePair<int, string>>();
	}

	/// <summary>
	/// Returns URI Path property part on given position (index).
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <param name="pathPartIndex"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetPathPart"
	, IsPrecise = true
	, DataAccess = DataAccessKind.None
	, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetPathPart(object sqlUriValue, int pathPartIndex) {
		return ValidateAndReturnClrUri(sqlUriValue).GetPathPart(pathPartIndex);
	}

	/// <summary>
	/// Returns first non-blank URI Path property part.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetFirstNonBlankPathPart"
	, IsPrecise = true
	, DataAccess = DataAccessKind.None
	, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetFirstNonBlankPathPart(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).GetFirstNonBlankPathPart();
	}

	/// <summary>
	/// Returns subpart of URI Path property, if given index and number of parts included are valid for such operation.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <param name="startIndex"></param>
	/// <param name="nbrOfPartsIncluded"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetPathSubpart"
	, IsPrecise = true
	, DataAccess = DataAccessKind.None
	, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetPathSubpart(object sqlUriValue, int startIndex, int nbrOfPartsIncluded) {
		return ValidateAndReturnClrUri(sqlUriValue).GetPathSubpart(startIndex, nbrOfPartsIncluded);
	}

	/// <summary>
	/// Returns index of first occurence of given part if exists as a Part in URI Path property.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <param name="pathPart"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetPathFirstIndexOfPart"
	, IsPrecise = true
	, DataAccess = DataAccessKind.None
	, SystemDataAccess = SystemDataAccessKind.None)]
	public static int? GetPathFirstIndexOfPart(object sqlUriValue, string pathPart) {
		return ValidateAndReturnClrUri(sqlUriValue).GetPathFirstIndexOfPart(pathPart);
	}

	/// <summary>
	/// Returns index of last occurence of given part if exists as a Part in URI Path property.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <param name="pathPart"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetPathLastIndexOfPart"
	, IsPrecise = true
	, DataAccess = DataAccessKind.None
	, SystemDataAccess = SystemDataAccessKind.None)]
	public static int? GetPathLastIndexOfPart(object sqlUriValue, string pathPart) {
		return ValidateAndReturnClrUri(sqlUriValue).GetPathLastIndexOfPart(pathPart);
	}

	public static void GetPathPartIndices_RowFunction(object row, out int index) {
		var indices = (int)row;
		index = indices;
	}

	/// <summary>
	/// Returns List of integers representing indices in URI Path Parts array for given Part.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <param name="pathPart"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetPathPartIndices"
		, TableDefinition = "[Index] int"
		, FillRowMethodName = nameof(GetPathPartIndices_RowFunction)
		, IsPrecise = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static System.Collections.IEnumerable GetPathPartIndices(object sqlUriValue, string pathPart) {
		var uri = ValidateAndReturnClrUri(sqlUriValue);
		return (System.Collections.IEnumerable)uri.GetPathPartIndices(pathPart) ?? Array.Empty<int>();
	}

	#endregion PathFunc

	#region QueryFunc

	/// <summary>
	/// Helper function defining result table for GetQueryParameters Table-valued function.
	/// </summary>
	/// <param name="row"></param>
	/// <param name="separator"></param>
	/// <param name="key"></param>
	/// <param name="hasAssignMark"></param>
	/// <param name="value"></param>
	public static void GetQueryParameters_RowFunction(object row, out char separator, out string key, out bool hasAssignMark, out string value) {
		var part = (UriQueryPart)row;
		separator = part.Separator;
		key = part.Key;
		hasAssignMark = part.HasAssignMark;
		value = part.Value;
	}

	/// <summary>
	/// Returns table with list of all URI Query property parameters and their values, 
	/// together with its separator and if parameter has assign mark.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetQueryParameterValues"
		, TableDefinition = "[Separator] nchar(1), [Key] nvarchar(max), [HasAssignMark] bit, [Value] nvarchar(max)"
		, FillRowMethodName = nameof(GetQueryParameters_RowFunction)
		, IsPrecise = true, DataAccess = DataAccessKind.None, SystemDataAccess = SystemDataAccessKind.None)]
	public static System.Collections.IEnumerable GetQueryParameters(object sqlUriValue) {
		var uri = ValidateAndReturnClrUri(sqlUriValue);
		return ((System.Collections.IEnumerable)uri.ParsedValue.Query.Parts) ?? Array.Empty<UriQueryPart>();
	}

	/// <summary>
	/// Returns value of URI Query parameter which was given.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <param name="queryParameter"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetQueryParameterValue"
		, IsPrecise = true
		, IsDeterministic = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetQueryParameterValue(object sqlUriValue, string queryParameter) {
		return ValidateAndReturnClrUri(sqlUriValue).GetQueryValue(queryParameter);
	}

	/// <summary>
	/// Returns value of URI Query parameter which was given, very fast.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <param name="queryParameter"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetQueryParameterValueFast"
		, IsPrecise = true
		, IsDeterministic = true
		, DataAccess = DataAccessKind.None
		, SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetQueryParameterValueFast(string sqlUriValue, string queryParameter) {
		if (string.IsNullOrEmpty(sqlUriValue)) { return null; }
		if (string.IsNullOrEmpty(queryParameter)) { return null; }

		var qp = queryParameter + "=";
		var lenQP = qp.Length;
		var startIndex = -1;
		while (0 <= (startIndex = sqlUriValue.IndexOf(qp, startIndex + 1))) {
			var prevCH = sqlUriValue[startIndex - 1];
			if (prevCH != '?' && prevCH != '&' && prevCH != ';') {
				// its another parameterer
				continue;
			}

			var ndxValueStartIndex = startIndex + lenQP;
			var ndxNext = sqlUriValue.IndexOfAny(new[] { '?', '&', ';', '#' }, ndxValueStartIndex);
			if (ndxNext < 0) {
				// last parameter
				return sqlUriValue.Substring(ndxValueStartIndex);
			}
			else {
				var len = ndxNext - ndxValueStartIndex;
				return sqlUriValue.Substring(ndxValueStartIndex, len);
			}
		}

		// parameter was not found
		return null;
	}

	#endregion QueryFunc 

	#region FragmentFunc

	/// <summary>
	/// Returns URI Fragment property value if valid.
	/// </summary>
	/// <param name="sqlUriValue"></param>
	/// <returns></returns>
	[SqlFunction(Name = "ClrUri::GetFragment"
	   , IsPrecise = true
	   , IsDeterministic = true
	   , DataAccess = DataAccessKind.None
	   , SystemDataAccess = SystemDataAccessKind.None)]
	public static string GetFragment(object sqlUriValue) {
		return ValidateAndReturnClrUri(sqlUriValue).ParsedValue.Fragment.ToString();
	}

	#endregion FragmentFunc
}
