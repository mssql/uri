using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using UriLib;
using UriLib.Support;
using Microsoft.SqlServer.Server;
using System.Text;

/// <summary>
/// Represents uniform resource identifier (uri/url/urn) with api friendly for usage in T-SQL.
/// </summary>
[SqlUserDefinedType(Format.UserDefined, IsFixedLength = false, MaxByteSize = -1, ValidationMethodName = nameof(ClrUri.Validate)), Serializable]
public partial struct ClrUri {
	private Url? parsedValue;
	/// <summary>
	/// Parsed value from <see cref="Original"/>.
	/// Returns NULL if <see cref="Original"/> is invalid.
	/// </summary>
	public Url ParsedValue => this.parsedValue ?? default(Url);

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="original">String value which can be valid or invalid uri.</param>
	public ClrUri(string original) : this() {
		this.parsedValue = Url.Parse(original);
	}

	public ClrUri(Url uri) : this() {
		this.parsedValue = uri;
	}

	/// <summary>
	/// Returns original string value.
	/// </summary>
	/// <returns></returns>
	public override string ToString() => this.ParsedValue.ToUriString();
}

/// <summary>
/// ClrUri Properties definition.
/// </summary>
partial struct ClrUri {

	public string Text => this.ParsedValue.ToUriString();
  
	public string Scheme => this.ParsedValue.Scheme;    
	public string SchemeName => this.ParsedValue.Scheme.Name;
    public bool SchemeIsValid => this.ParsedValue.Scheme.IsValid;
	public bool SchemeIsEmpty => this.ParsedValue.Scheme.IsEmpty;

	public string UserName => this.ParsedValue.UserInfo.Name;
    public bool UserNameIsEmpty => this.ParsedValue.UserInfo.Name.IsEmpty;
	public string UserPassword => this.ParsedValue.UserInfo.Password;
    public bool UserPasswordIsEmpty => this.ParsedValue.UserInfo.Password.IsEmpty;
	public string UserInfo => this.ParsedValue.UserInfo.ToUriString();
	public bool UserInfoIsEmpty => this.ParsedValue.UserInfo.IsEmpty;
    public bool UserInfoIsValid => this.ParsedValue.UserInfo.IsValid;
    
	public string Host => this.ParsedValue.Host.ToUriString();
	public bool HostIsEmpty => this.ParsedValue.Host.IsEmpty;
    public bool HostIsValid => this.ParsedValue.Host.IsValid;

	public string Port => this.ParsedValue.Port.ToUriString();
	public int? PortNumber => this.ParsedValue.Port;
	public bool PortIsEmpty => !this.ParsedValue.Port.IsEmpty;
	public bool PortIsValid => this.ParsedValue.Port.IsValid;
	public bool PortIsNumber => this.ParsedValue.Port.IsNumber;

	public string HostWithPort {
		get {
			var port = this.PortNumber;
			if (port.HasValue) {
				return $"{this.Host}:{port}";
			}
			else {
				return this.Host;
			}
		}
	}

	public string Path => this.ParsedValue.Path.Text;
	public bool PathIsEmpty => this.ParsedValue.Path.IsEmpty;
    public bool PathIsValid => this.ParsedValue.Path.IsValid;
	public int PathPartsCount => this.ParsedValue.Path.PartsCount;

	public string Query => this.ParsedValue.Query.Text;
	public bool QueryIsEmpty => this.ParsedValue.Query.IsEmpty;
    public bool QueryIsValid => this.ParsedValue.Query.IsValid;
	public int QueryPartsCount => this.ParsedValue.Query.PartsCount;

	public string Fragment => this.ParsedValue.Fragment;
	public bool FragmentIsEmpty => this.ParsedValue.Fragment.IsEmpty;
    public bool FragmentIsValid => this.ParsedValue.Fragment.IsValid;


    //public string LocalPath => this.parsedValue?.GetLocalPath();
    //public string Authority => this.parsedValue?.GetAuthority();
    //public string HostNameType => this.parsedValue?.GetHostNameType().ToString();
    //public bool? IsDefaultPort => this.parsedValue?.GetIsDefaultPort();
    //public bool? IsFile => this.parsedValue?.GetIsFile();
    //public bool? IsLoopback => this.parsedValue?.GetIsLoopback();
    //public string PathAndQuery => this.parsedValue?.GetPathAndQuery();

    // TODO: somehow solve returning arrays
    // public string[] Segments;

    //public bool? IsUnc => this.parsedValue?.GetIsUnc();
    //public bool? IsAbsoluteUri => this.parsedValue?.GetIsAbsoluteUri();
    //public int? Port => this.parsedValue?.GetPort();
    //public string Query => this.parsedValue?.GetQuery();
    //public string Fragment => this.parsedValue?.GetFragment();
    //public string Scheme => this.parsedValue?.GetScheme();
    //public string DnsSafeHost => this.parsedValue.GetDnsSafeHost();
    //public string IdnHost => this.parsedValue?.GetIdnHost();
    //public string AbsoluteUri => this.parsedValue?.GetAbsoluteUri();
    //public string Host => this.parsedValue?.GetHost();
    //public string AbsolutePath => this.parsedValue?.GetAbsolutePath();
    //public string UserInfo => this.parsedValue?.GetUserInfo();
    //public bool? UserEscaped => this.parsedValue?.GetUserEscaped();
}

/// <summary>
/// Functions for URI Path property.
/// </summary>
partial struct ClrUri {

   /// <summary>
   /// Returns Key-Value Array of URI Path parts and their indices.
   /// </summary>
   /// <returns></returns>
    public KeyValuePair<int, string>[] GetPartParts()
    {
        var path = this.ParsedValue.Path;       
        var parts = path.Parts;

        if (parts is null)
        {
            return null;
        }
        var partsCount = parts.Count;

        if (!path.IsValid || path.IsEmpty)
        {
            return null;
        }       
        else
        {
            var result = new KeyValuePair<int, string>[partsCount];
            for (int i = 0; i < partsCount; i++)
            {
                result[i] = new KeyValuePair<int, string>(i, parts[i].Text);
            }
            return result;
        }

    }

    /// <summary>
    /// Return Path part on given index.
    /// </summary>
    /// <param name="pathPartIndex"></param>
    /// <returns></returns>
    public string GetPathPart(int pathPartIndex)
    {
        var path = this.ParsedValue.Path;
        var parts = path.Parts;

        if (!path.IsValid || path.IsEmpty)
        {
            return null;
        }
        else if(parts.Count <= pathPartIndex || pathPartIndex < 0)
        {
            return null;
        }
        else
        {            
           return parts[pathPartIndex].Text;
        }     
    }

    /// <summary>
    /// Return first non-blank Path part.
    /// </summary>
    /// <returns></returns>
    public string GetFirstNonBlankPathPart()
    {
        var path = this.ParsedValue.Path;
        var parts = path.Parts;

        if(!path.IsValid || path.IsEmpty)
        {
            return null;
        }        
        else
        {
            for(int i = 0; i < parts.Count; i++)
            {
                if (parts[i].IsEmpty) { continue; }
                else { return parts[i]; } 
            }
            return null;
        }
    } 

    /// <summary>
    /// Returns subpart of Path if given index and number of parts included are valid.
    /// </summary>
    /// <param name="startIndex"></param>
    /// <param name="nbrOfPartsIncluded"></param>
    /// <returns></returns>
    public string GetPathSubpart(int startIndex, int nbrOfPartsIncluded)
    {
        var path = this.ParsedValue.Path;
        var parts = path.Parts;
        var lastIndex = startIndex + (nbrOfPartsIncluded - 1);

        if (startIndex < 0 || nbrOfPartsIncluded <= 0)
        {
            return null;
        }       
        else if(!path.IsValid || path.IsEmpty)
        {
            return null;
        }       
        else if(startIndex >= parts.Count || parts.Count < lastIndex || parts.Count == 0)
        {
            return null;
        }
        else
        {
            var result = new StringBuilder(); 
            for(int i = startIndex; i <= lastIndex; i++)
            {
                result.Append(parts[i]);
                if (i == lastIndex) { return result.ToString(); }
                result.Append("/");
            }
            return result.ToString();
        }       
    }

    /// <summary>
    /// Returns index of first occurence of given part if exists as a Part in URI Path property.
    /// </summary>
    /// <param name="pathPart"></param>
    /// <returns></returns>
    public int? GetPathFirstIndexOfPart(string pathPart)
    {
        var path = this.ParsedValue.Path;
        var parts = path.Parts;

        if (pathPart is null)
        {
            return null;
        }
        else if (!path.IsValid || path.IsEmpty)
        {
            return null;
        }
        else
        {
            for(int i = 0; i < parts.Count; i++)
            {
                if (0 == string.Compare(pathPart, parts[i].ToString(), StringComparison.Ordinal))
                {
                    return i;
                }                
            }
            return null;
        }
    }

    /// <summary>
    /// Returns index of last occurence of given part if exists as a Part in URI Path property.
    /// </summary>
    /// <param name="pathPart"></param>
    /// <returns></returns>
    public int? GetPathLastIndexOfPart(string pathPart)
    {
        var path = this.ParsedValue.Path;
        var parts = path.Parts;

        if (pathPart is null)
        {
            return null;
        }
        else if (!path.IsValid || path.IsEmpty)
        {
            return null;
        }
        else
        {            
            for (int i = parts.Count - 1; i >= 0; i--)
            {
                if (0 == string.Compare(pathPart, parts[i].ToString(), StringComparison.Ordinal))
                {
                    return i;
                }
            }
            return null;
        }
    }

    /// <summary>
    /// Returns List of integers representing indices in URI Path Parts array for given Part.
    /// </summary>
    /// <param name="pathPart"></param>
    /// <returns></returns>
    public int[] GetPathPartIndices(string pathPart)
    {
        var path = this.ParsedValue.Path;
        var parts = path.Parts;

        if(pathPart is null)
        {
            return null;
        }
        else if(!path.IsValid || path.IsEmpty)
        {
            return null;
        }
        else
        {
            var indices = new List<int>();
            for (int i = 0; i < parts.Count; i++)
            {
                if (0 == string.Compare(pathPart, parts[i].ToString(), StringComparison.Ordinal))
                {
                    indices.Add(i);
                }                
            }
            return indices.Count == 0 ? null : indices.ToArray();
        }       
    }
}

/// <summary>
/// Functions for URI Query property.
/// </summary>
partial struct ClrUri {
    /// <summary>
    /// Returns true if given PARAMETER is in query and has valid .
    /// </summary>
    /// <param name="parameterName"></param>
    /// <returns></returns>
	public bool HasQueryValue(string parameterName) {
		if (string.IsNullOrEmpty(parameterName)) { return false; }

		var part = this.ParsedValue.Query.FindQueryPart(parameterName);
		return part.HasValue;
	}

   /// <summary>
   /// Returns VALUE of first occurence of given PARAMETER in Query property, if exists.
   /// </summary>
   /// <param name="parameterName"></param>
   /// <returns></returns>
	public string GetQueryValue(string parameterName) {
		if (string.IsNullOrEmpty(parameterName)) { return null; }

		var part = this.ParsedValue.Query.FindQueryPart(parameterName);
		return part?.Value;
	}	  

    /// <summary>
    /// Returns VALUE of last occurence of given PARAMETER in query property, if exists.
    /// </summary>
    /// <param name="parameterName"></param>
    /// <returns></returns>
    public string GetQueryValueLast(string parameterName)
    {
        if(string.IsNullOrEmpty(parameterName)) { return null; }
        var query = this.ParsedValue.Query.Parts;
        if(query is null) { return null; }
        for(int i = query.Count - 1; i >= 0; i--)
        {
            if(0 == string.Compare(parameterName, query[i].Key, StringComparison.Ordinal))
            {
                return query[i].Value.ToString();
            }
        }
        return null;
    }    

    /// <summary>
    /// Retruns URI Query parameters and their values as JSON.
    /// </summary>
    /// <param name="parameterName"></param>
    /// <returns></returns>
    public string GetQueryValuesAsJSON(string parameterName)
    {
        if (string.IsNullOrEmpty(parameterName)) { return null; }
        var query = this.ParsedValue.Query;

		return query.GetQueryValuesByKey(parameterName).ToJsonArray();
    }
   
}

partial struct ClrUri {
	/// <summary>
	/// Returns TRUE if value is valid URI.
	/// Othervise returns FALSE.
	/// </summary>
	public bool Validate() => this.IsNull || !(this.parsedValue is null);

	/// <summary>
	/// Returns TRUE if value is valid URI.
	/// Othervise returns FALSE.
	/// </summary>
	public bool IsValid {
		get {
			var value = this.parsedValue;
			if (!value.HasValue) { return true; }

			return value.Value.IsValid();
		}
	}

	/// <summary>
	/// Returns TRUE if value is NOT valid URI.
	/// Othervise returns FALSE.
	/// </summary>
	public bool IsInvalid => !this.IsValid;

	/// <summary>
	/// Returns TRUE if <paramref name="value"/> is valid uri.
	/// Othervise returns FALSE.
	/// </summary>
	/// <param name="value">Tested value.</param>
	/// <returns></returns>
	public static bool IsValidUri(string value) => System.Uri.IsWellFormedUriString(value, UriKind.RelativeOrAbsolute);
}

partial struct ClrUri : INullable {
	/// <summary>
	/// Returns TRUE if this value represents NULL.
	/// Othervise returns FALSE.
	/// </summary>
	public bool IsNull => !this.parsedValue.HasValue;

	/// <summary>
	/// Returns value representing NULL.
	/// </summary>
	public static ClrUri Null => new ClrUri();

	public static ClrUri Empty => new ClrUri(string.Empty);

	/// <summary>
	/// Parses string value to <see cref="ClrUri"/> type.
	/// </summary>
	/// <param name="value">String value which can be valid or invalid uri.</param>
	public static ClrUri Parse(SqlString value) => new ClrUri(value.Value);
}

partial struct ClrUri : IBinarySerialize {
	/// <summary>
	/// Reads binary data into this value.
	/// </summary>
	/// <param name="r">Reader of binary data.</param>
	public void Read(BinaryReader r) {
		var version = r.ReadByte();
		if (0 == version) {
			this.parsedValue = null;
		}
		else if (1 == version) {
			this.ReadVersion1(r);
		}
		else {
			var errMsg = string.Format("Can not read serialized value of version '{0}'.", version);
			throw new NotSupportedException(errMsg);
		}
	}

	/// <summary>
	/// Reads binary data formated in "version 1".
	/// </summary>
	/// <param name="r">Reader of binary data.</param>
	private void ReadVersion1(BinaryReader r) {
		var str = r.ReadString();
		this.parsedValue = Url.Parse(str);
	}

	/// <summary>
	/// Writes current value into binary data.
	/// </summary>
	/// <param name="w">Writer of binary data.</param>
	public void Write(BinaryWriter w) {
		if (this.IsNull) {
			w.Write((byte)0);
		}
		else {
			w.Write((byte)1);
			this.WriteVersion1(w);
		}

		w.Flush();
	}

	/// <summary>
	/// Writes current value into binary data in "version 1" format.
	/// </summary>
	/// <param name="w">Writer of binary data.</param>
	private void WriteVersion1(BinaryWriter w) {
		w.Write(this.Text);
	}
}
