using System;
using System.Collections.Generic;
using System.Text;

partial struct ClrUri {
	#region Scheme
	public ClrUri WithScheme(string scheme) => new ClrUri(this.ParsedValue.WithScheme(scheme));
	public ClrUri WithoutScheme() => new ClrUri(this.ParsedValue.WithoutScheme());
	#endregion Scheme
}
