﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace ClrUriLib.BENCHMARK {
	public class Program {
		static void Main(string[] args) {
			// var summary = BenchmarkRunner.Run<StringSubstring_VS_StrSpan__HelloWorld>();
			var summary = BenchmarkRunner.Run<StringSubstring_VS_StrSpan__LoremIpsum>();
		}
	}

	public class StringSubstring_VS_StrSpan__HelloWorld {
		private static readonly string HelloWorld = "Hello world!";

		[Benchmark]
		public string Substring() => HelloWorld.Substring(3, 5);

		[Benchmark]
		public StrSpan Span() => new StrSpan(HelloWorld, 3, 5);
	}

	public class StringSubstring_VS_StrSpan__LoremIpsum {
		private static readonly string HelloWorld = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Curabitur bibendum justo non orci. Duis viverra diam non justo. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Praesent dapibus. Curabitur bibendum justo non orci. In rutrum. Mauris metus. Aliquam ornare wisi eu metus. Aliquam erat volutpat. Aenean placerat. In convallis. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Etiam posuere lacus quis dolor. Integer malesuada. Mauris metus. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Fusce suscipit libero eget elit.";

		[Benchmark]
		public string Substring() => HelloWorld.Substring(3, 55);

		[Benchmark]
		public StrSpan Span() => new StrSpan(HelloWorld, 3, 55);
	}
}
