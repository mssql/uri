go

set statistics io on
set statistics time on

SELECT  [ValidTestURI],
		[dbo].[ClrUri::GetQueryParameter]([ValidTestURI], N'q') AS [QueryValue]
FROM [dbo].[PerformanceClrURITest];

SELECT  [ValidTestURI],
		SUBSTRING([ValidTestURI], PATINDEX('%q=%', [ValidTestURI]) + 2, PATINDEX('%&as_eq=%', [ValidTestURI]) - PATINDEX('%q=%', [ValidTestURI]) - 2) AS [QueryValue]
FROM [dbo].[PerformanceClrURITest]
WHERE PATINDEX('%&as_eq=%', [ValidTestURI]) - (PATINDEX('%q=%', [ValidTestURI]) + 2 ) > 0;

set statistics io off
set statistics time off
